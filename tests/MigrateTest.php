<?php

use Mockery as m;
use Fh\Deployment\Migrate;

class MigrateTest extends PHPUnit_Framework_TestCase {

    private function newMigrate($expected_batch_number = 9) {
        $configPath = __DIR__."/../database_test.php";
        $migrationDir = __DIR__."/../migrations";
        $m = m::mock(
            'Fh\Deployment\Migrate[getMaxBatch]'
            ,[
                $expected_batch_number
                ,$configPath
                ,$migrationDir
                ,'enterprisedb'
            ]
        );
        $m->setConfig('testing', true);
        return $m;
    }

    public function testGetConfig() {
        $m = $this->newMigrate();
        $expected = [
            'driver' => 'mysql'
            ,'database' => 'enterprise_db'
            ,'username' => 'ent_db_user'
            ,'host' => 'local-db.fh.org'
            ,'password' => 'password'
            ,'charset' => 'utf8'
            ,'collation' => 'utf8_unicode_ci'
            ,'prefix' => ''
        ];

        $ret = $m->getDbConfig();
        $this->assertEquals($expected,$ret);
    }

    public function testGetDbManager() {
        $m = $this->newMigrate();
        $m->init();
        $c = $m->getConnection('enterprisedb');
        $expectedClass = 'Illuminate\Database\MySqlConnection';
        $this->assertEquals($expectedClass,get_class($c));
    }

    public function testGetDirectionDown() {
        $m = $this->newMigrate(8);
        $m->init();
        $m->shouldReceive('getMaxBatch')->andReturn(9);
        $dir = $m->getDirection();
        $this->assertEquals('down',$dir);
    }

    public function testGetDirectionUp() {
        $m = $this->newMigrate(10);
        $m->init();
        $m->shouldReceive('getMaxBatch')->andReturn(9);
        $dir = $m->getDirection();
        $this->assertEquals('up',$dir);
    }

    public function testGetDirectionStay() {
        $m = $this->newMigrate(9);
        $m->init();
        $m->shouldReceive('getMaxBatch')->andReturn(9);
        $dir = $m->getDirection();
        $this->assertEquals('stay',$dir);
    }

    public function testGetDirectionError() {
        $m = $this->newMigrate(11);
        $m->init();
        $m->shouldReceive('getMaxBatch')->andReturn(9);
        $dir = $m->getDirection();
        $this->assertEquals('error',$dir);
        $expectedMessages = ["The target database is more than one step behind the target release. Cannot increment the batch number more than 1 in a single release."];
        $this->assertEquals($expectedMessages,$m->getMessages());
    }

    public function testMigrateUp() {
        $m = $this->newMigrate(10);
        $m->init();
        $m->shouldReceive('getMaxBatch')->andReturn(9);

        $m->expectCommand('php artisan migrate --path=/home/jwatson/Source/localdev/api2.fh.org/vendor/fh/migration-sync/tests/../migrations --force')
            ->andReturn("\n");

        ob_start();
        $m->main();
        $out = ob_get_contents();
        ob_end_clean();
        $expected = "Released migration: 10
Database max batch: 9
Expected migration batch number is 1 ahead of database. Migrating forward.
command: php artisan migrate --path=/home/jwatson/Source/localdev/api2.fh.org/vendor/fh/migration-sync/tests/../migrations --force


";
        $this->assertEquals($expected,$out);
    }

    public function testMigrateDownOnce() {
        $m = $this->newMigrate(8);
        $m->init();
        $m->shouldReceive('getMaxBatch')->twice()->andReturn(9,8);

        $m->expectCommand('php artisan migrate:rollback --force')
            ->andReturn("\n");

        ob_start();
        $m->main();
        $out = ob_get_contents();
        ob_end_clean();
        $expected = "Released migration: 8
Database max batch: 9
command: php artisan migrate:rollback --force


Released migration: 8
Database max batch: 8
Expected migration batch number matches database max batch number.
Nothing to migrate.
";
        $this->assertEquals($expected,$out);
    }

    public function testMigrateDownTwice() {
        $m = $this->newMigrate(7);
        $m->init();
        $m->shouldReceive('getMaxBatch')->times(3)->andReturn(9,8,7);

        $m->expectCommand('php artisan migrate:rollback --force')
            ->andReturn("\n");
        $m->expectCommand('php artisan migrate:rollback --force')
            ->andReturn("\n");

        ob_start();
        $m->main();
        $out = ob_get_contents();
        ob_end_clean();
        $expected = "Released migration: 7
Database max batch: 9
command: php artisan migrate:rollback --force


Released migration: 7
Database max batch: 8
command: php artisan migrate:rollback --force


Released migration: 7
Database max batch: 7
Expected migration batch number matches database max batch number.
Nothing to migrate.
";
        $this->assertEquals($expected,$out);
    }

    public function testMigrateFailure() {
        $m = $this->newMigrate(7);
        $m->init();
        $m->shouldReceive('getMaxBatch')->times(3)->andReturn(9,8,8);

        $m->expectCommand('php artisan migrate:rollback --force')
            ->andReturn("\n");
        $m->expectCommand('php artisan migrate:rollback --force')
            ->andReturn("\n");

        ob_start();
        $m->main();
        $out = ob_get_contents();
        ob_end_clean();
        $expected = "Released migration: 7
Database max batch: 9
command: php artisan migrate:rollback --force


Released migration: 7
Database max batch: 8
command: php artisan migrate:rollback --force


Released migration: 7
Database max batch: 8
Detected migration failure. Further recursion aborted.
";
        $this->assertEquals($expected,$out);
    }

    public function testMigrateStay() {
        $m = $this->newMigrate(9);
        $m->init();
        $m->shouldReceive('getMaxBatch')->andReturn(9);

        ob_start();
        $m->main();
        $out = ob_get_contents();
        ob_end_clean();
        $expected = "Released migration: 9
Database max batch: 9
Expected migration batch number matches database max batch number.
Nothing to migrate.
";
        $this->assertEquals($expected,$out);
    }

    public function testMigrateError() {
        $m = $this->newMigrate(11);
        $m->init();
        $m->shouldReceive('getMaxBatch')->andReturn(9);

        ob_start();
        $m->main();
        $out = ob_get_contents();
        ob_end_clean();
        $expected = "Released migration: 11
Database max batch: 9
The target database is more than one step behind the target release. Cannot increment the batch number more than 1 in a single release.
Migration aborted. No work done.
";
        $this->assertEquals($expected,$out);
    }

}
