<?php

namespace Fh\Deployment;

use Illuminate\Database\Connector;
use Illuminate\Database\Capsule\Manager as Capsule;

class Migrate extends CommandRunner {

    /*
     * Array for storing error messages
     */
    protected $messages = [];

    /*
     * Capsule instance so we can use Illuminate\Database outside of Laravel
     */
    protected $capsule = null;

    /*
     * The batch number associated with this release
     */
    protected $expected_batch = 999999999; // Safest to throw error by default causing no action.

    /*
     * The max batch number from the database migrations table
     */
    protected $database_max_batch = 999999999; // Again cause no change by default.

    /*
     * The max batch number from the database migrations table from the previous recursive call.
     */
    protected $last_database_max_batch = 999999999; // Again cause no change by default.

    /*
     * Array to hold configuration data
     */
    protected $config = [];

    /*
     * String name of connection found in your application configuration file.
     */
    protected $strConnectionName = 'default';

    /*
     * Full server path to your migrations directory
     */
    protected $migrationDir = '/tmp';

    /**
     * Sets up the migration class with initialized member vars
     *
     * @param $released_batch_number integer from a file that is
     *        released with source code to indicate the migration
     *        that is released with this code.
     * @param $strDbConfigFilePath string full server path to the
     *        laravel database.php config file.
     * @param $strMigrationDir string full server path to the
     *        migrations directory where migrations are held.
     * @param $strConnectionName string name of DB connection in
     *        question from your config/database.php file.
     */
    public function __construct($released_batch_number, $strDbConfigFilePath, $strMigrationDir, $strConnectionName) {
        $this->capsule = new Capsule;
        $this->expected_batch = $released_batch_number;
        $this->config = require $strDbConfigFilePath;
        $this->setConfig('testing',false); // Needs to be initialized
        $this->strConnectionName = $strConnectionName;
        $this->migrationDir = $strMigrationDir;
    }

    /**
     * Usage statement
     *
     * Echoes usage details to STDOUT.
     *
     * @return void
     */
    public static function usage() {
        echo "\n";
        echo "Migration Syncronization Controller\n";
        echo "\n";
        echo "Usage:\n";
        echo "\n";
        echo "migrate-sync batch config migrationDir connection\n";
        echo "\n";
        echo "batch        integer the laravel batch number that this code release is tied to.\n";
        echo "config       path to your laravel database config file. Example: config/database.php\n";
        echo "migrationDir path to the directory where your migrations live. Example: vendor/mycompany/migrations\n";
        echo "connection   string name of the connection in your database config file that contains the migrations table.\n";
        echo "\n";
        echo "\n";
    }

    /**
     * Initialize the connection
     * @return void
     */
    public function init() {
        $this->addConnection();
        $this->bootEloquent();
    }

    /**
     * Add an error message
     * @param $str string message to add to the array
     * @return void
     */
    public function addMessage($str) {
        $this->messages[] = $str;
    }

    /**
     * Return the messages array
     * @return array
     */
    public function getMessages() {
        return $this->messages;
    }

    /**
     * Return the database configuration array for use in the Capsule
     * @return array
     */
    public function getDbConfig() {
        return $this->config['connections'][$this->strConnectionName];
    }

    /**
     * Adds a connection to the Capsule's database manager
     * @return void
     */
    public function addConnection() {
        $dbConfig = $this->getDbConfig();
        $c = $this->capsule->getContainer();
        $c['config']['database.default'] = $this->strConnectionName;
        $this->capsule->addConnection($dbConfig,$this->strConnectionName);
    }

    /**
     * Initializes Eloquent so we can use it if we want it
     * @return void
     */
    public function bootEloquent() {
        $this->capsule->setAsGlobal();
        $this->capsule->bootEloquent();
    }

    /**
     * Returns a connection object that we can call random
     * queries on.
     * @return \Illuminate\Database\Connection
     */
    public function getConnection() {
        return $this->capsule->getConnection($this->strConnectionName);
    }

    /**
     * Queries the database to get the max batch number
     * from the migrations table.
     * @return integer max batch number
     */
    public function getMaxBatch() {
        $c = $this->getConnection();

        $rows = $c->select("SELECT MAX(batch) AS batch FROM migrations");
        $row = $rows[0];
        $maxBatch = $row->batch;
        return $maxBatch;
    }

    /**
     * Gets the direction we should migrate.
     * Valid direction return values are:
     *
     * up
     * down
     * stay
     * error
     *
     * If the return value is 'error', then a message
     * will be populated in the $this->messages array.
     * You can get the message with $this->getMessages().
     */
    public function getDirection() {
        $dbMaxBatch = $this->database_max_batch = $this->getMaxBatch();
        if($dbMaxBatch < $this->expected_batch)
        {
            $difference = $this->expected_batch - $dbMaxBatch;
            if($difference == 1)
            {
                return 'up';
            }
            else
            {
                $this->addMessage('The target database is more than one step behind the target release. Cannot increment the batch number more than 1 in a single release.');
                return 'error';
            }
        }
        if($dbMaxBatch == $this->expected_batch)
            return 'stay';
        if($dbMaxBatch > $this->expected_batch)
            return 'down';
    }

    /**
     * Main entry point for process logic.
     *
     * Logic explained:
     *
     * If the database max batch is exactly 1 less than the expected
     * release batch, then we want to migrate forward with
     * php artisan migrate
     *
     * If the database max batch is 2 or more less than the expected
     * release batch, then we need to throw an error because
     * php artisan migrate will run all migrations in a single batch
     * and increment once, thus never bringing the database up
     * to the expected release number. This reflects a misunderstanding
     * of the release, and should be addressed before continuing.
     * Most likely, the release just needs to reflect the actual
     * target batch number (current db max batch +1)
     *
     * If the database is ahead of the expected release batch,
     * then we need to migrate down with
     * php artisan rollback
     * We need to do this as many times as is necessary to
     * roll back to the expected release batch number.
     */
    public function main() {

        $direction = $this->getDirection();
        $this->out('Released migration: '.$this->expected_batch);
        $this->out('Database max batch: '.$this->database_max_batch);

        if($this->database_max_batch == $this->last_database_max_batch) {
            $this->out("Detected migration failure. Further recursion aborted.");
            return;
        }

        $this->last_database_max_batch = $this->database_max_batch;

        if($direction == 'stay') {
            $this->out('Expected migration batch number matches database max batch number.');
            $this->out('Nothing to migrate.');
        }

        if($direction == 'error') {
            $this->out(implode("\n",$this->getMessages()));
            $this->out('Migration aborted. No work done.');
            return;
        }

        if($direction == 'up') {
            $this->out('Expected migration batch number is 1 ahead of database. Migrating forward.');
            $command = 'php artisan migrate';
            if($this->migrationDir) {
                $command .= ' --path=' . $this->migrationDir;
            }
            $command .= ' --force';
            $this->out($this->command($command));
            return;
        }

        if($direction == 'down') {
            $command = 'php artisan migrate:rollback --force';
            $this->out($this->command($command));
            $this->main(); // recursion for multiple rollbacks
            return;
        }

    }

}
