#!/usr/bin/env php
<?php

use Fh\Deployment\Migrate;

require_once(__DIR__."/../../autoload.php");
require_once(__DIR__."/function.env.php");

if(count($argv) != 5) {
    echo Migrate::usage();
    exit;
}

$m = new Migrate($argv[1], $argv[2], $argv[3], $argv[4]);

try {
    $m->init();
    $m->main();
} catch (\Exception $e) {
    echo "Exception encountered: " . $e->getMessage() . ' in file ' . $e->getFile() . ' on line ' . $e->getLine();
}
