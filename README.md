# fh/migration-sync

## The Problem Described

In our development environments, we have 4 promotion levels:

- alpha
- beta
- stage
- production

The thought process is simple: Developers own alpha. They work their art to create marvelous things there. They also work with the release team to iron out a deployment process. As they develop this deployment process, the software ends up in beta. So beta is the completion of the first development cycle, which includes a first stab at a deployment.

At that point, release management comes into the picture. They can't go from beta to production because they need to practice the release and make sure all of the knowledge about releasing this software was recorded and communicated. So stage is the place to practice the deployment.

Typically, we try to keep stage as close to production as we possibly can, including database, server packages installed, code base, etc. And we iron out any problems that might come up as we do that. We continually roll back everything in stage to its pre-release state before we try again to make sure we're practicing a production release in stage every time.

After that, we should have reasonable confidence that we can deploy to production without risk because we just deployed to a mirror of production and it worked.

But what if it doesn't work? What if, for some reason (which turns out to be more common than anyone would like), we failed to make stage reflect production enough? We need to roll-back to an earlier code base, reverse migrations, etc.

For that matter, we expect that stage deployments are going to fail a whole lot more often than production deployments, right? Shouldn't we have a solid roll-back process for that too so each practice release to stage is practicing the whole release? Incremental releases accumulate in beta. Stage isn't the place for that. Stage needs to roll-back to the state that production is in if anything failed, or if any new increment is introduced to the release. If we don't do this, we run a risk, however large or small, of running into something in production that wasn't accurately reflected in stage.

## Partial Solutions That Already Exist

So [git](https://git-scm.com/) has a great solution for this when it comes to deploying code. You can set up a gitolite server on the server that hosts your project, and use server [side git hooks](https://bitbucket.org/jon_watson/fh-git-hooks) to push to it. Your server side git hook can execute a [deployment process](https://bitbucket.org/jon_watson/git-deployment) that pushes your newly pushed code out into your directory.

Not only that, but if you want to roll-back, this same deployment process will still work. Let's say you have a release branch called 3.0. You've merged all of your feature branches into it and you have deployed it to beta. Then you find out that one of your features is broken and won't be fixed until the 3.1 version. So, no problem. Assuming you tagged 3.0 with a tag called 3.0.0 when you created it, you simply:

```sh
$ git checkout 3.0
$ git reset --hard 3.0.0
$ git merge 3.0-some-feature-that-is-not-broken
$ git merge 3.0-some-other-feature-that-is-not-broken
$ echo "And we will decidedly *NOT* merge the feature that is broken."
```

Now we have a nice, clean 3.0 branch that doesn't have the bad feature in it. And we can push that to our deployment remote (stage) with:

```sh
$ git push -f stage 3.0
```

The -f flag forces the remote to accept the push, because the stage remote might see it as a non-fast-forward because it was rolled-back. We *know* it's a non-fast-forward, and we want it in stage anyway. So we force it.

## Great! So what's the problem?

### Database. Migrations.

We use [Laravel](http://laravel.com/docs/5.1/migrations) for our migrations because it offers an easy to use up() and down() system that allows you to migrate and roll-back database changes easily. The problem is that my deployment process doesn't know whether it's being rolled-back or advanced. The code is just deployed.

Part of the deployment process is to run migrations with the old familiar:

```sh
$ php artisan migrate
```

But now I want to roll-back. I could run:

```sh
$ php artisan migrate:rollback
```

But there is a problem with this approach:

- If I'm rolling back, how does my deployment script know how many times to run the rollback?

We could easily assume that we are always only going to run a single rollback. But what if we had a problem with the database schema that caused us to run three or four forward-migrations (trying to fix a problem) before finally deciding to rollback to a working database schema five versions ago as a last resort? Then we need to know how many times to run the migrate:rollback.

To compound the situation, Laravel migrations run in batches, so as developers are creating their migrations individually, they will run them individually, thus creating individual batch number increments for each new migration they run. It might look like this in alpha:

|`migration`|`batch`|
|-----------|-------|
|2015_04_02_134606_remove_unused_tables|1|
|2015_04_02_145445_fix_GrantDataSubmission_polymorphism_gbo|1|
|2015_04_14_134204_Grants_Gbo_Refactor|2|
|2015_07_06_172604_OrganizationTables|2|
|2015_07_14_131932_AddStatesToReferenceList|3|
|2015_02_04_204054_PrettyOfficeNames|4|
|2015_08_03_175158_create_Prospect_tables|5|

But since releases to stage and production happen so rarely, it might look like this in stage:

|`migration`|`batch`|
|-----------|-------|
|2015_04_02_134606_remove_unused_tables|1|
|2015_04_02_145445_fix_GrantDataSubmission_polymorphism_gbo|1|
|2015_04_14_134204_Grants_Gbo_Refactor|1|
|2015_07_06_172604_OrganizationTables|1|
|2015_07_14_131932_AddStatesToReferenceList|2|
|2015_02_04_204054_PrettyOfficeNames|3|
|2015_08_03_175158_create_Prospect_tables|3|

The batch number increments once for each time artisan migrate finds any number of new migrations to run. Whatever new migrations it finds, it puts all of them into the same batch.

This isn't really all that friendly for migration rollbacks. If I roll-back from v3.0.27 to version v3.0.22, how many times do I need to run php artisan migrate:rollback for the database schema to match what my code requires for that version?

## A Strategy

### Addressing the sync problem

First, we have to address the synchronization issue between production and the other promotion levels (alpha, beta, stage).

We can do this in a couple of different ways. The easiest, and most stable way is to copy the production database down to your other environments. If your promotion sequence is anything like ours, then you are probably already doing this on a semi-regular basis anyway.

If copying your production database down to your other environments is not an option, cheat, and just edit your migrations table to make the batch numbers match.

### Addressing the number of rollbacks to execute

What if we could tell each tagged version of the code which batch it should be at right now?

Let's say we release version 3.0.24, and that version knows its current max batch number *should be* `5`? Then, what if it could look at the database to see that the max batch number in the database is `4`? At that point, my deployment script could *expect* a forward migration.

Conversely, if we are deploying version 3.0.22, and the code knows its current max batch number should be `3`. But the database says it's at `4`. At that point, I could *expect* a rollback instead of a forward migration. Further, if I find that my code says it should be at `3` and my database is at `5`, then I know I need to run the rollback twice.

Of course, if the number in the database matches what the code things it should be, then neither a rollback nor a forward migration should be attempted.

An error case might be if the database is at `4`, and my code says it should be at `6`. Since a migration batch will only increment once, that should throw an error.

### Current migration file versioned with source code

File: current_migration_batch

We can easily tell the code what batch this version of the code should be at by simply committing a file to our repository that has the expected batch number for this release. So whenever I create a new migration, I check with my team to let them know I am incrementing the batch number for a new release. This lets the rest of the team know that if they create any new migrations for this coming production release, they don't need to increment it. 

Remember, we want a single increment for each batch of migrations that will be installed in production. So we need to coordinate as a team.

## The Code

My deployment process follows the [fh-git-hooks](https://bitbucket.org/jon_watson/fh-git-hooks) strategy, using [fh-git-deployment](https://bitbucket.org/jon_watson/git-deployment.git) as its delivery mechanism. I have a post-deployment script configured called ./deploy.sh. So after the code has been deployed, the ./deploy.sh script handles other tasks like:

- composer install
- set directory permissions so apache can write to logs
- git submodule init/update
- run forward or rollback migrations
- restart memcached
- restart apache just in case any environment variables changed
- etc.

Here's the php file that kicks off the migration process:

```php
<?php
// FILE: do_migrations.php
require_once(__DIR__ . "vendor/autoload.php");

use Fh\Deployment\MigrationHelper;
$batch = file_get_contents("current_migration_batch");
MigrationHelper::migrate($batch);
```

So the MigrationHelper::migrate() method does the all of the magic logic described above, comparing the $batch variable from this current code version to the max batch number in the database, and either executes a forward migration or a (one or more) rollbacks accordingly.

## Communication

With this method of managing migration planning and rollbacks, communication on the team critical. But it might end up being easier than you might think for mid-sized teams with well-planned releases.

For example:

- Let's say we have release branch 3.0. The current_migration_batch_file number is 3.
- Jerry creates feature branch 3.0-A.
- Janice creates feature branch 3.0-B.
- Both create a migration.
- Assuming that their alpha environments are in sync with production when they started, 3.0 should have the correct batch number checked into the current_migration_batch file.
- So both Jerry and Janice can increment the current_migration_batch file number.
- Jerry ends up with 4.
- Janice also ends up with 4.

If both features are going into the 3.0 release, everyone is happy by accident. Both migrations end up being part of the same release, and the batch number was incremented only once as it should be.

## Fringe Cases

Some fringe cases might exist:

1. If you have two projects with separate migrations acting on the same database. If this is the case, then you have no idea what order your migrations will be in or which batch they will belong to. Don't do this. Never keep two different sets of migrations that will act on the same database.
2. When you're working on something several versions ahead. For example, let's say your production environment is at version 3.0. Jerry is working on version 3.1, and Janice is working on 3.2, and she has no idea whether Jerry is going to create a migration in his work. It's possible that Janice could have the batch number changed out from under her. With team communication, and awareness of what is going into production when 3.1 gets released, this is an easy problem to solve. Janice can always pull previous versions into your code as they go to production. Since Jerry is expected to complete 3.1 before Janice completes 3.2, she pulls in Jerry's work after he goes to production and increments her current_migration_batch file from there. 




