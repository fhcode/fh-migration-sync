<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAreatypeTypenameColumnSize extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('AreaType'))
        {
            Schema::table('AreaType', function (Blueprint $table) {

                if(Schema::hasColumn('AreaType', 'TypeName'))
                {
                    /*
                     * Since there is no way of altering the column structure with the schema builder,
                     * we are obliged to use the internal DB functionality
                     */
                    DB::statement('ALTER TABLE AreaType MODIFY COLUMN TypeName VARCHAR(255)');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('AreaType'))
        {
            Schema::table('AreaType', function (Blueprint $table) {

                if(Schema::hasColumn('AreaType', 'TypeName'))
                {
                    /*
                     * Since there is no way of altering the column structure with the schema builder,
                     * we are obliged to use the internal DB functionality
                     */
                    DB::statement('ALTER TABLE AreaType MODIFY COLUMN TypeName VARCHAR(50)');
                }
            });
        }
    }
}
