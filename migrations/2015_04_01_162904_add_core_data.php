<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Fh\Data\Console\Seed\DataSeeder;
use Fh\Data\Gbo\BusinessObjects\Role;
use Fh\Data\Gbo\BusinessObjects\Permission;


class AddCoreData extends Migration {

    public $tables =
        [
            'roles',
            'permissions',
            'App',
            'Area',
            'BusinessUnit',
            'ManagedRoute',
            'ManagedRouteTranslation',
            'Office',
            'ReferenceList',
            'ReferenceListTranslation',
            'Worker',
            'User',
            'assigned_roles',
            'AssignedRoleArea'
        ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $table) {
            $this->addData($table);
        }

        $this->setPermissionsToRole();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //not going to write a down - as it would only delete all data - feels dangerous to have that around.
        //rolling back all migrations has same effect if really needing to start with clean DB
    }

    public function addData($table)
    {
        $dataSeeder = new DataSeeder();

        //this migration may be sometimes run against a live, populated database - in which case don't
        //want to add anything at all.
        $count = DB::table($table)->count();

        if ($count === 0) {
            $dataSeeder->importData($table, __DIR__ . '/Data/');
        }
    }


    private function setPermissionsToRole()
    {

        $RolePermissions =
            array(
                'Super Admin' => array(
                    'gr.home',
                    'gr.projects',
                    'gr.projects.detail',
                    'gr.monitoringdata',
                    'gr.evaluationdata',
                    'gr.projects.data',
                    'gr.reports',
                    'ad.translations',
                    'ad.translatedata',
                    'ad.translatedatasave',
                    'ad.home',
                    'ad.routes',
                    'ad.routesave',
                    'gr.monitoringdata.postform',
                    'gr.monitoringdata.getstart',
                    'gr.monitoringdata.poststart',
                    'gr.monitoringdata.getform',
                    'gr.evaluationdata.postform',
                    'gr.evaluationdata.getstart',
                    'gr.evaluationdata.poststart',
                    'gr.evaluationdata.getform',
                    'gr.evaluationdata.manageall',
                    'gr.monitoringdata.manageall',
                    'gr.data.delete',
                    'gr.data.reopen',
                    'gr.project.indicators',
                    'gr.indicator.targets',
                    'gr.targets.save',
                    'gr.project.indicators.json',
                    'gr.project.indicators.detail',
                    'gr.project.objective.detail'
                ),
                'gr.Administrator' => array(
                    'gr.home',
                    'gr.projects',
                    'gr.projects.detail',
                    'gr.monitoringdata',
                    'gr.evaluationdata',
                    'gr.projects.data',
                    'gr.reports',
                    'gr.monitoringdata.postform',
                    'gr.monitoringdata.getstart',
                    'gr.monitoringdata.poststart',
                    'gr.monitoringdata.getform',
                    'gr.evaluationdata.postform',
                    'gr.evaluationdata.getstart',
                    'gr.evaluationdata.poststart',
                    'gr.evaluationdata.getform',
                    'gr.evaluationdata.manageall',
                    'gr.monitoringdata.manageall',
                    'gr.data.delete',
                    'gr.data.restore',
                    'gr.data.reopen',
                    'gr.project.indicators',
                    'gr.indicator.targets',
                    'gr.targets.save',
                    'gr.project.indicators.json',
                    'gr.project.indicators.detail',
                    'gr.project.objective.detail'
                ),
                'gr.Manager' => array(
                    'gr.home',
                    'gr.projects',
                    'gr.projects.detail',
                    'gr.monitoringdata',
                    'gr.evaluationdata',
                    'gr.projects.data',
                    'gr.monitoringdata.postform',
                    'gr.monitoringdata.getstart',
                    'gr.monitoringdata.poststart',
                    'gr.monitoringdata.getform',
                    'gr.monitoringdata.manageall',
                    'gr.evaluationdata.postform',
                    'gr.evaluationdata.getstart',
                    'gr.evaluationdata.poststart',
                    'gr.evaluationdata.getform',
                    'gr.data.delete',
                    'gr.data.restore',
                    'gr.data.reopen',
                ),
                'gr.Contributor' => array(
                    'gr.home',
                    'gr.projects',
                    'gr.projects.detail',
                    'gr.projects.data',
                    'gr.monitoringdata.postform',
                    'gr.monitoringdata.getstart',
                    'gr.monitoringdata.poststart',
                    'gr.monitoringdata.getform',
                ),
                'ad.Translator' => array(
                    'ad.home',
                    'ad.translations',
                    'ad.translatedata',
                    'ad.translatedatasave',
                    'gr.monitoringdata.postform',
                    'gr.monitoringdata.getstart',
                    'gr.monitoringdata.poststart',
                    'gr.monitoringdata.getform'
                ),
                'ad.RouteManager' => array(
                    'ad.home',
                    'ad.routes',
                    'ad.routesave'
                )
            );


        $count = DB::table('permission_role')->count();

        //only do this if the table is currently empty
        if ($count === 0) {
            foreach ($RolePermissions as $Role => $Permissions) {
                $pIDs = array();
                //get the ids for each permission listed as associated with the role
                foreach ($Permissions as $Permission) {
                    $p = Permission::where('name', '=', $Permission)->first();
                    $pIDs[] = $p->id;
                }

                //get a Role object and assign the permissions to it
                $r = Role::where('name', '=', $Role)->first();
                $r->perms()->sync($pIDs);
            }
        }
    }

}
