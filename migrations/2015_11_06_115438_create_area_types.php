<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Fh\Data\Mapper\US\AreaTypeMapper;

class CreateAreaTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $areaTypes = [
            ["Globe", 'Globe'],
            ["GlobalRegion", 'Global Region'],
            ["Country",'Country'],
            ["Cluster", "Cluster"],
            ["Community", "Community"],
            ["Woreda", "Woreda"]
        ];

        foreach ($areaTypes as $type) {
            $exist = AreaTypeMapper::where('AreaTypeCode', '=', $type[0])->exists();

            if(!$exist)
            {
                $areaType = new AreaTypeMapper();
                $areaType->AreaTypeCode = $type[0];
                $areaType->TypeName = $type[1];

                $areaType->save();
            }
        }

        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        $areaTypes = [
            ["Globe", 'Globe'],
            ["GlobalRegion", 'Global Region'],
            ["Country",'Country'],
            ["Cluster", "Cluster"],
            ["Community", "Community"],
            ["Woreda", "Woreda"]
        ];

        foreach ($areaTypes as $type) {
            $exist = AreaTypeMapper::where('AreaTypeCode', '=', $type[0])->exists();

            if($exist)
            {
                AreaTypeMapper::where('AreaTypeCode','=', $type[0])->delete();
            }
        }


        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
