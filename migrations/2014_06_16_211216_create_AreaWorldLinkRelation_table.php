<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaWorldLinkRelationTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AreaWLRelation', function (Blueprint $table) {
            $table->integer('AreaId')->unsigned();
            $table->string('code', 10);
            $table->string('table', 20);
            $table->string('fo_org_code', 10);
            $table->timestamps();

            $table->primary('AreaId');
        });

        $sql = "CREATE VIEW `cdp_center` AS
            select
                `AreaWLRelation`.`code` AS `center_code`,
                `AreaWLRelation`.`fo_org_code`,
                `Area`.`AreaName` AS `center_name`
        FROM AreaWLRelation
        inner join Area on
        `AreaWLRelation`.`AreaId` = `Area`.`AreaId`
        where `Area`.`TypeName` = 'Community';";

        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AreaWLRelation');
        DB::statement("DROP VIEW IF EXISTS cdp_center");
    }

}
