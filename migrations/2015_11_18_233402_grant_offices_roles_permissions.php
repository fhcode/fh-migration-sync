<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Fh\Data\Gbo\BusinessObjects\Role;
use Fh\Data\Gbo\BusinessObjects\Permission;
use Fh\Data\Gbo\BusinessObjects\App;

class GrantOfficesRolesPermissions extends Migration
{
    private $app = ['WL3', 'WL'];

    private $roles  = [
        ['reporter', 'Reporter'],
        ['grant_administrator','Grant Administrator'],
        ['grant_contributor','Grant Contributor']
    ];

    private $permissions = [
        ['main.offices',  'offices', 'WL3'],
        ['main.officesDetail',  'office Detail', 'WL3'],
        ['main.officesCreate',  'office Create', 'WL3']
    ];

    //all permissions related to UI screens that the user will be able to see
    private $uiPermissions = [
        'Super Admin' =>
            [
                'main.offices',
                'main.officesDetail',
                'main.officesCreate'
            ],
        'grant_administrator' =>
            [
                'main.offices',
                'main.officesetail',
                'main.officesCreate'
            ],
        'grant_contributor' =>
            [
                'main.offices',
                'main.officesDetail'
            ],
        'reporter' =>
            [
                'main.offices',
                'main.officesDetail'
            ]
    ];


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //just in case, create the app that the permissions are related to
        $this->createApp($this->app[0], $this->app[1]);

        //create all roles that don't already exist
        foreach ($this->roles as $role) {
            $this->createRole($role[0], $role[1]);
        }

        //create all permissions that don't already exist.
        foreach ($this->permissions as $permission) {
            $this->createPermission($permission[0], $permission[1], $permission[2]);
        }

        //assign permissions to each role
        foreach ($this->uiPermissions as $role=>$permissionArray) {
            foreach ($permissionArray as $permission) {
                $this->assignPermissions($permission, $role);
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //this migration may have added new roles and permissions, but we cannot delete them all
        //because those roles may have already existed before this migration, and if so, then we don't
        //want to go ahead and delete them.

        //unassign permssions from each role
        foreach ($this->uiPermissions as $role=>$permissionArray) {
            foreach ($permissionArray as $permission) {
                $this->unassignPermissions($permission, $role);
            }
        }

    }

    private function createRole($name, $display)
    {
        $existing = Role::where('name', $name)->first();

        if (!is_object($existing)) {
            $role = new Role();
            $role->name = $name;
            $role->display_name = $display;
            $role->save();
            return $role;
        } else {
            return null;
        }
    }

    private function createApp($title, $code)
    {
        $existing = App::where('Title', $title)->first();

        if (!is_object($existing)) {
            $app = new App();
            $app->Title = $title;
            $app->Code = $code;
            $app->save();
            return $app;
        } else {
            return null;
        }
    }

    private function createPermission($name, $display, $app)
    {
        $existing = Permission::where('name', $name)->first();

        $app = App::where('Title', $app)->first();

        if (!is_object($existing)) {
            $permission = new Permission();
            $permission->name = $name;
            $permission->display_name = $display;
            $permission->AppId = $app->AppId;
            $permission->save();
            return $permission;
        } else {
            return null;
        }

    }

    private function assignPermissions($permissionName, $roleName)
    {
        //get role object
        $role = Role::where('name', $roleName)->first();

        //get permission
        $permission = Permission::where('name', $permissionName)->first();

        if (is_object($role) && is_object($permission)) {
            //attach permission to role
            $role->attachPermission($permission);
            return true;
        } else {
            return false;
        }

    }

    private function unassignPermissions($permissionName, $roleName)
    {
        //get role object
        $role = Role::where('name', $roleName)->first();

        //get permission
        $permission = Permission::where('name', $permissionName)->first();


        if (is_object($role) && is_object($permission)) {
            //detach permission from role
            $role->perms()->detach($permission->id);
            return true;
        } else {
            return false;
        }

    }

}
