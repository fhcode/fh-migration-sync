<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllowedParentAreaTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('AllowedParentAreaType'))
        {
            Schema::create('AllowedParentAreaType', function (Blueprint $table) {

                $table->increments("AllowedParentAreaTypeId");
                $table->string('AreaTypeCode', 30);
                $table->string('ParentAreaTypeCode', 50);
                $table->boolean("Required");

                if(Schema::hasTable('AreaType'))
                    $table->foreign('AreaTypeCode')->references('AreaTypeCode')->on('AreaType');

                if(Schema::hasTable('AreaType'))
                    $table->foreign('ParentAreaTypeCode')->references('AreaTypeCode')->on('AreaType');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AllowedParentAreaType');
    }
}
