<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Fh\Data\Dao\US\ReferenceList;

class InsertCostPaymentTypeReferenceListData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
         * translations are not provided
         */
        $list = [
            ["Cost Reimbursement", "Cost Reimbursement"],
            ["Advances" ,"Advances"],
            ["Other","Other"]
        ];
        $root = ReferenceList::where('Code', '=', 'CostPaymentType')->first();
        if(!is_object($root))
        {
            $root = new ReferenceList();
            $root->Description = 'Cost Payment Type';
            $root->Code = 'CostPaymentType';
            $root->Active = true;

            $root->save();
        }

        //$root = ReferenceList::where('Description', '=', 'Cost Payment Type')->first();

        foreach ($list as $type) {

            $child = ReferenceList::where('Description', '=', $type[0])->where('ParentId', '=', $root->ReferenceListId)->first();

            if(!is_object($child))
            {

                $child = new ReferenceList();

                $child->Description = $type[0];
                $child->Depth = 1;
                $child->Active = true;

                $child->save();

                $child->makeChildOf($root);

                $child->translateOrNew('en')->Description = $type[0];
                //$child->translateOrNew('es')->Description = $type[1];

                $child->save();
            }
        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        $list = [
            "Cost Reimbursement",
            "Advances",
            "Other"
        ];

        $root = ReferenceList::where('Code', '=', 'CostPaymentType')->first();

        if(is_object($root))
        {
            foreach ($list as $type) {
                $child = ReferenceList::where('Description', '=', '')->where('ParentId', '=', $root->ReferenceListId)->first();

                if(is_object($child))
                {
                    ReferenceListTranslation::where('ReferenceListId','=', $child->ReferenceListId)->where('Description', '=', $type)->delete();
                    $child->delete();
                }

            }
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
