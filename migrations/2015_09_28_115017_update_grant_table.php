<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGrantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Grant', function (Blueprint $table) {

            if(!Schema::hasColumn('Grant', 'Status'))
                $table->string('Status', 50)->nullable();

            if(!Schema::hasColumn('Grant', 'FundingType'))
                $table->integer('FundingType')->unsigned();



            if(!Schema::hasColumn('Grant', 'DueDate'))
                $table->dateTime('DueDate')->nullable();

            if(!Schema::hasColumn('Grant', 'Amount'))
                $table->decimal('Amount',19,2)->nullable();

            if(!Schema::hasColumn('Grant', 'GSCContact'))
                $table->integer('GSCContact')->unsigned()->nullable();

            if(!Schema::hasColumn('Grant', 'FinanceContact'))
                $table->integer('FinanceContact')->unsigned()->nullable();

            if(!Schema::hasColumn('Grant', 'CountryContact'))
                $table->integer('CountryContact')->unsigned()->nullable();

            if(!Schema::hasColumn('Grant', 'DirectCost'))
                $table->decimal('DirectCost',19,2)->unsigned()->nullable();

            if(!Schema::hasColumn('Grant', 'DirectCost'))
                $table->decimal('GSCIndirect',19,2)->unsigned()->nullable();

            if(!Schema::hasColumn('Grant', 'MatchRequired'))
                $table->integer('MatchRequired')->unsigned()->nullable();

            if(!Schema::hasColumn('Grant', 'CostShareType'))
                $table->integer('CostShareType')->unsigned()->nullable();

            if(!Schema::hasColumn('Grant', 'CostShareComment'))
                $table->text('CostShareComment')->nullable();

            if(!Schema::hasColumn('Grant', 'CostPaymentType'))
                $table->integer('CostPaymentType')->unsigned()->nullable();

            if(!Schema::hasColumn('Grant', 'LOACash'))
                $table->decimal('LOACash',19,2)->nullable();

            if(!Schema::hasColumn('Grant', 'LOAGIK'))
                $table->decimal('LOAGIK',19,2)->nullable();

            if(!Schema::hasColumn('Grant', 'CommodityOcean'))
                $table->decimal('CommodityOcean',19,2)->nullable();

            if(!Schema::hasColumn('Grant', 'CommodityInland'))
                $table->decimal('CommodityInland',19,2)->nullable();

            if(!Schema::hasColumn('Grant', 'AgreementCode'))
                $table->integer('AgreementCode')->unsigned()->nullable();

            if(!Schema::hasColumn('Grant', 'BusinessUnit'))
                $table->string('BusinessUnit', 10)->nullable();

            if(!Schema::hasColumn('Grant', 'CFDA'))
                $table->integer('CFDA')->unsigned()->nullable();

            if(!Schema::hasColumn('Grant', 'GeoCode'))
                $table->integer('GeoCode')->unsigned()->nullable();

            if(!Schema::hasColumn('Grant', 'FinanceDescription'))
                $table->text('FinanceDescription')->nullable();

            if(!Schema::hasColumn('Grant', 'WorkshopNeeded'))
                $table->boolean('WorkshopNeeded')->nullable();

            if(!Schema::hasColumn('Grant', 'LeadUnit'))
                $table->integer('LeadUnit')->unsigned()->nullable();

            if(!Schema::hasColumn('Grant', 'Sector'))
                $table->integer('Sector')->unsigned()->nullable();

            if(!Schema::hasColumn('Grant', 'Subsector'))
                $table->integer('Subsector')->unsigned()->nullable();

            if(Schema::hasTable('ReferenceList') && Schema::hasColumn('ReferenceList', 'ReferenceListId'))
                if(Schema::hasColumn('Grant', 'FundingType'))
                    $table->foreign('FundingType')->references('ReferenceListId')->on('ReferenceList');

            if(Schema::hasTable('Worker') && Schema::hasColumn('Worker', 'WorkerId'))
                if(Schema::hasColumn('Grant', 'GSCContact'))
                    $table->foreign('GSCContact')->references('WorkerId')->on('Worker');

            if(Schema::hasTable('Worker') && Schema::hasColumn('Worker', 'WorkerId'))
                if(Schema::hasColumn('Grant', 'FinanceContact'))
                    $table->foreign('FinanceContact')->references('WorkerId')->on('Worker');

            if(Schema::hasTable('Worker') && Schema::hasColumn('Worker', 'WorkerId'))
                if(Schema::hasColumn('Grant', 'CountryContact'))
                    $table->foreign('CountryContact')->references('WorkerId')->on('Worker');

            if(Schema::hasTable('ReferenceList') && Schema::hasColumn('ReferenceList', 'ReferenceListId'))
                if(Schema::hasColumn('Grant', 'MatchRequired'))
                    $table->foreign('MatchRequired')->references('ReferenceListId')->on('ReferenceList');

            if(Schema::hasTable('ReferenceList') && Schema::hasColumn('ReferenceList', 'ReferenceListId'))
                if(Schema::hasColumn('Grant', 'CostShareType'))
                    $table->foreign('CostShareType')->references('ReferenceListId')->on('ReferenceList');


            if(Schema::hasTable('ReferenceList') && Schema::hasColumn('ReferenceList', 'ReferenceListId'))
                if(Schema::hasColumn('Grant', 'CostPaymentType'))
                    $table->foreign('CostPaymentType')->references('ReferenceListId')->on('ReferenceList');


            if(Schema::hasTable('ReferenceList') && Schema::hasColumn('ReferenceList', 'ReferenceListId'))
                if(Schema::hasColumn('Grant', 'LeadUnit'))
                    $table->foreign('LeadUnit')->references('ReferenceListId')->on('ReferenceList');

            if(Schema::hasTable('ReferenceList') && Schema::hasColumn('ReferenceList', 'ReferenceListId'))
                if(Schema::hasColumn('Grant', 'Sector'))
                    $table->foreign('Sector')->references('ReferenceListId')->on('ReferenceList');

            if(Schema::hasTable('ReferenceList') && Schema::hasColumn('ReferenceList', 'ReferenceListId'))
                if(Schema::hasColumn('Grant', 'Subsector'))
                    $table->foreign('Subsector')->references('ReferenceListId')->on('ReferenceList');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Grant', function (Blueprint $table) {

            if(Schema::hasColumn('Grant', 'Status'))
                $table->dropColumn('Status');

            if(Schema::hasColumn('Grant', 'FundingType')){
                //$table->dropForeign('grant_FundingType_foreign');
                $table->dropColumn('FundingType');
            }
            if(Schema::hasColumn('Grant', 'DueDate'))
                $table->dropColumn('DueDate');

            if(Schema::hasColumn('Grant', 'Amount'))
                $table->dropColumn('Amount');

            if(Schema::hasColumn('Grant', 'GSCContact')){
                //$table->dropForeign('grant_GSCContact_foreign');
                $table->dropColumn('GSCContact');
            }

            if(Schema::hasColumn('Grant', 'FinanceContact'))
            {
                //$table->dropForeign('grant_FinanceContact_foreign');
                $table->dropColumn('FinanceContact');
            }

            if(Schema::hasColumn('Grant', 'CountryContact'))
            {
                //$table->dropForeign('grant_CountryContact_foreign');
                $table->dropColumn('CountryContact');
            }

            if(Schema::hasColumn('Grant', 'DirectCost'))
                $table->dropColumn('DirectCost');


            if(Schema::hasColumn('Grant', 'DirectCost'))
                $table->dropColumn('GSCIndirect');

            if(Schema::hasColumn('Grant', 'MatchRequired'))
            {
                //$table->dropForeign('grant_MatchRequired_foreign');
                $table->dropColumn('MatchRequired');
            }

            if(Schema::hasColumn('Grant', 'CostShareType'))
            {
                //$table->dropForeign('grant_CostShareType_foreign');
                $table->dropColumn('CostShareType');
            }

            if(Schema::hasColumn('Grant', 'CostShareComment'))
                $table->dropColumn('CostShareComment');

            if(Schema::hasColumn('Grant', 'CostPaymentType'))
            {
                //$table->dropForeign('grant_CostPaymentType_foreign');
                $table->dropColumn('CostPaymentType');
            }

            if(Schema::hasColumn('Grant', 'LOACash'))
                $table->dropColumn('LOACash');

            if(Schema::hasColumn('Grant', 'LOAGIK'))
                $table->dropColumn('LOAGIK');

            if(Schema::hasColumn('Grant', 'CommodityOcean'))
                $table->dropColumn('CommodityOcean');

            if(Schema::hasColumn('Grant', 'CommodityInland'))
                $table->dropColumn('CommodityInland');

            if(Schema::hasColumn('Grant', 'AgreementCode'))
                $table->dropColumn('AgreementCode');

            if(Schema::hasColumn('Grant', 'BusinessUnit'))
                $table->dropColumn('BusinessUnit');

            if(!Schema::hasColumn('Grant', 'CFDA'))
                $table->dropColumn('CFDA');

            if(!Schema::hasColumn('Grant', 'GeoCode'))
                $table->dropColumn('GeoCode');

            if(!Schema::hasColumn('Grant', 'FinanceDescription'))
                $table->dropColumn('FinanceDescription');

            if(!Schema::hasColumn('Grant', 'WorkshopNeeded'))
                $table->dropColumn('WorkshopNeeded');

            if(Schema::hasColumn('Grant', 'LeadUnit'))
            {
                //$table->dropForeign('grant_LeadUnit_foreign');
                $table->dropColumn('LeadUnit');
            }

            if(Schema::hasColumn('Grant', 'Sector'))
            {
                //$table->dropForeign('grant_Sector_foreign');
                $table->dropColumn('Sector');
            }

            if(Schema::hasColumn('Grant', 'Subsector'))
            {
                //$table->dropForeign('grant_Subsector_foreign');
                $table->dropColumn('Subsector');
            }


        });
    }
}
