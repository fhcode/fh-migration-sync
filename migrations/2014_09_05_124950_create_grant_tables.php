<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrantTables extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GrantProject', function (Blueprint $table) {
                $table->increments('GrantProjectId');
                $table->string('Name', 255);
                $table->integer('HostCountryId')->unsigned();
                $table->decimal('Budget', 19, 4)->nullable();
                $table->string('AreaAggregateLevel', 20);
                $table->string('AreaReportingLevel', 20);
                $table->dateTime('StartDate')->nullable();
                $table->dateTime('EndDate')->nullable();
                $table->integer('MaleBeneficiaries')->unsigned()->nullable();
                $table->integer('FemaleBeneficiaries')->unsigned()->nullable();
                $table->integer('ChildBeneficiaries')->unsigned()->nullable();
                $table->integer('TotalBeneficiaries')->unsigned()->nullable();
                $table->text('Description')->nullable();

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('HostCountryId')->references('AreaId')->on('Area');
        });

        Schema::create('GrantPromoter', function (Blueprint $table) {
                $table->increments('GrantPromoterId');
                $table->integer('WorkerId')->unsigned();
                $table->integer('GrantProjectId')->unsigned();

                $table->timestamps();

                $table->foreign('WorkerId')->references('WorkerId')->on('Worker');
                $table->foreign('GrantProjectId')->references('GrantProjectId')->on('GrantProject');
        });

        Schema::create('GrantDonor', function (Blueprint $table) {
                $table->increments('GrantDonorId');

                $table->string('Name', 255);

                $table->timestamps();
                $table->softDeletes();
        });

        Schema::create('Grant', function (Blueprint $table) {
                $table->increments('GrantId');
                $table->integer('GrantProjectId')->unsigned();
                $table->integer('GrantDonorId')->unsigned();

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('GrantProjectId')->references('GrantProjectId')->on('GrantProject');
                $table->foreign('GrantDonorId')->references('GrantDonorId')->on('GrantDonor');
        });

        Schema::create('GrantObjective', function (Blueprint $table) {
                $table->increments('GrantObjectiveId');

                //Baum hierarchy columns
                $table->integer('ParentId')->nullable();
                $table->integer('LeftIndex')->nullable();
                $table->integer('RightIndex')->nullable();
                $table->integer('Depth')->nullable();
                $table->integer('GrantProjectId')->unsigned();
                $table->integer('GrantObjectiveTypeTitleId')->unsigned();
                $table->string('GrantObjectiveType', 50);
                $table->string('Title', 500);
                $table->integer('SectorId')->unsigned();

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('GrantProjectId')->references('GrantProjectId')->on('GrantProject');
                $table->foreign('GrantObjectiveTypeTitleId')->references('ReferenceListId')->on('ReferenceList');
                $table->foreign('SectorId')->references('ReferenceListId')->on('ReferenceList');
        });

        Schema::create('GrantObjectiveTranslation', function (Blueprint $table) {
                $table->increments('GrantObjectiveTranslationId');
                $table->integer('GrantObjectiveId')->unsigned();
                $table->string('Title', 500);
                $table->string('Locale', 10)->index();
                $table->unique(['GrantObjectiveId', 'Locale']);
                $table->foreign('GrantObjectiveId')
                    ->references('GrantObjectiveId')->on('GrantObjective')->onDelete('cascade');
                $table->timestamps();
        });

        Schema::create('GrantServiceArea', function (Blueprint $table) {
                $table->increments('GrantServiceAreaId');
                $table->integer('GrantProjectId')->unsigned();
                $table->integer('AreaId')->unsigned();

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('GrantProjectId')->references('GrantProjectId')->on('GrantProject');
                $table->foreign('AreaId')->references('AreaId')->on('Area');
        });

        Schema::create('GrantDataSubmission', function (Blueprint $table) {
                $table->increments('GrantDataSubmissionId');
                $table->integer('GrantProjectId')->unsigned();
                $table->integer('AreaId')->unsigned();
                $table->integer('PromoterId')->unsigned();
                $table->integer('UserId')->unsigned();
                $table->date('Timeframe');
                $table->string('Status', 16);
                $table->integer('PeriodicityId');
                $table->string('PeriodicityType', 100);
                $table->string('Type', 50);

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('GrantProjectId')->references('GrantProjectId')->on('GrantProject');
                $table->foreign('AreaId')->references('AreaId')->on('Area');
                $table->foreign('PromoterId')->references('WorkerId')->on('Worker');
                $table->foreign('UserId')->references('UserId')->on('User');
        });

        Schema::create('GrantDataSubmissionItem', function (Blueprint $table) {
                $table->increments('GrantDataSubmissionItemId');
                $table->integer('GrantDataSubmissionId')->unsigned();
                $table->integer('AssignmentId')->unsigned();
                $table->string('AssignmentType', 100);
                $table->string('Value1', 1000);
                $table->string('Value2', 1000);

                $table->foreign('GrantDataSubmissionId')->references('GrantDataSubmissionId')->on('GrantDataSubmission');
            });


        Schema::create('GrantIndicator', function (Blueprint $table) {
                $table->increments('GrantIndicatorId');
                $table->string('InputTitle', 500);
                $table->string('ReportTitle', 500);
                $table->string('Type', 16);
                $table->integer('UnitId')->unsigned();
                $table->string('Formula', 500);
                $table->string('VerificationMeans', 500);
                $table->boolean('Disaggregate')->nullable();
                $table->string('CalculationType', 50);
                $table->string('ReportTag', 1000);

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('UnitId')->references('ReferenceListId')->on('ReferenceList');
        });

        Schema::create('GrantIndicatorTranslation', function (Blueprint $table) {
                $table->increments('GrantIndicatorTranslationId');
                $table->integer('GrantIndicatorId')->unsigned();
                $table->string('InputTitle', 500);
                $table->string('ReportTitle', 500);
                $table->string('Locale', 10)->index();
                $table->unique(['GrantIndicatorId', 'Locale']);
                $table->timestamps();

                $table->foreign('GrantIndicatorId')
                    ->references('GrantIndicatorId')->on('GrantIndicator')->onDelete('cascade');
        });

        Schema::create('GrantProjectPeriod', function (Blueprint $table) {
                $table->increments('GrantProjectPeriodId');
                $table->integer('GrantProjectId')->unsigned();
                $table->date('StartDate');
                $table->date('EndDate');
                $table->string('Title', 500);

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('GrantProjectId')->references('GrantProjectId')->on('GrantProject');
        });


        Schema::create('GrantIndicatorAssignment', function (Blueprint $table) {
                $table->increments('GrantIndicatorAssignmentId');
                $table->integer('GrantObjectiveId')->unsigned();
                $table->integer('GrantIndicatorId')->unsigned();
                $table->string('IndicatorType', 25);
                $table->string('ReportTag', 1000);

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('GrantObjectiveId')->references('GrantObjectiveId')->on('GrantObjective');
                $table->foreign('GrantIndicatorId')->references('GrantIndicatorId')->on('GrantIndicator');
        });


        Schema::create('GrantIndicatorPeriod', function (Blueprint $table) {
                $table->increments('GrantIndicatorPeriodId');
                $table->integer('GrantIndicatorAssignmentId')->unsigned();
                $table->integer('GrantProjectPeriodId')->unsigned();

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('GrantIndicatorAssignmentId')
                    ->references('GrantIndicatorAssignmentId')->on('GrantIndicatorAssignment')
                    ->onUpdate('CASCADE')->onDelete('CASCADE');
                $table->foreign('GrantProjectPeriodId')->references('GrantProjectPeriodId')->on('GrantProjectPeriod');
        });

        Schema::create('GrantTarget', function (Blueprint $table) {
                $table->increments('GrantTargetId');
                $table->integer('GrantTargetRelationId')->unsigned();
                $table->integer('AreaId')->unsigned();
                $table->string('GrantTargetType', 75);
                $table->decimal('Value', 19, 4);
                $table->date('Date');

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('AreaId')->references('AreaId')->on('Area');
        });

        Schema::create('GrantIndicatorItemList', function (Blueprint $table) {
                $table->increments('GrantIndicatorItemListId');
                $table->string('Title', 255);
                $table->boolean('Disaggregate')->nullable();

                $table->timestamps();
                $table->softDeletes();
        });

        Schema::create('GrantIndicatorItemListTranslation', function (Blueprint $table) {
                $table->increments('GrantIndicatorTranslationId');
                $table->integer('GrantIndicatorItemListId')->unsigned();
                $table->string('Title', 255);
                $table->string('Locale', 10)->index();
                $table->timestamps();
                $table->foreign('GrantIndicatorItemListId', 'grantindicatoritemlisttranslation_foreign')
                    ->references('GrantIndicatorItemListId')->on('GrantIndicatorItemList')->onDelete('cascade');
                $table->unique(
                    ['GrantIndicatorItemListId', 'Locale'],
                    'grantindicatoritemlisttranslation_locale_unique'
                );
        });

        Schema::create('GrantIndicatorItems', function (Blueprint $table) {
                $table->increments('GrantIndicatorItemsId');
                $table->integer('GrantIndicatorAssignmentId')->unsigned();
                $table->integer('GrantIndicatorItemListId')->unsigned();

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('GrantIndicatorAssignmentId')
                    ->references('GrantIndicatorAssignmentId')->on('GrantIndicatorAssignment')
                    ->onDelete('CASCADE')->onUpdate('CASCADE');
                $table->foreign('GrantIndicatorItemListId')
                    ->references('GrantIndicatorItemListId')->on('GrantIndicatorItemList');
        });

        Schema::create('GrantIndicatorItemsDefault', function (Blueprint $table) {
                $table->increments('GrantIndicatorItemsDefaultId');
                $table->integer('GrantIndicatorId')->unsigned();
                $table->integer('GrantIndicatorItemListId')->unsigned();

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('GrantIndicatorId')->references('GrantIndicatorId')->on('GrantIndicator');
                $table->foreign('GrantIndicatorItemListId')
                    ->references('GrantIndicatorItemListId')->on('GrantIndicatorItemList');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('GrantPromotor');
        Schema::dropIfExists('GrantPromoter');
        Schema::dropIfExists('GrantProject');
        Schema::dropIfExists('Grant');
        Schema::dropIfExists('GrantDonor');
        Schema::dropIfExists('GrantObjective');
        Schema::dropIfExists('GrantObjectiveTranslation');
        Schema::dropIfExists('GrantActivity');
        Schema::dropIfExists('GrantActivityTranslation');
        Schema::dropIfExists('GrantActivityUnit');
        Schema::dropIfExists('GrantActivityUnitAssignment');
        Schema::dropIfExists('GrantActivityUnitTranslation');
        Schema::dropIfExists('GrantServiceArea');
        Schema::dropIfExists('GrantDataSubmission');
        Schema::dropIfExists('GrantDataSubmissionItem');
        Schema::dropIfExists('GrantIndicatorAssignment');
        Schema::dropIfExists('GrantTarget');
        Schema::dropIfExists('GrantIndicator');
        Schema::dropIfExists('GrantIndicatorTranslation');
        Schema::dropIfExists('GrantProjectPeriod');
        Schema::dropIfExists('GrantIndicatorPeriod');
        Schema::dropIfExists('GrantIndicatorItems');
        Schema::dropIfExists('GrantIndicatorItemsDefault');
        Schema::dropIfExists('GrantIndicatorItemList');
        Schema::dropIfExists('GrantIndicatorItemListTranslation');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
