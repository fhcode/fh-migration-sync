<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentGatewayTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Only create the table if it doesn't exist
        if (!Schema::hasTable('PaymentGatewayTransaction'))
        {
            Schema::create('PaymentGatewayTransaction', function ($t) {
                $t->increments('PaymentGatewayTransactionId');
                $t->string('VendorCode', 20);
                $t->string('TransactionId', 255)->unique();
                $t->dateTime('TransactionDate')->index('idx_TransactionDate');
                $t->string('PaymentType', 20);
                $t->string('TransactionType', 20);
                $t->decimal('TransactionAmount', 19, 4);
                $t->string('CurrencyCode', 4)->nullable();
                $t->string('InternalId', 255)->nullable();
                $t->string('AuthorizationMessage', 255)->nullable();
                $t->string('AuthorizationCode', 255)->nullable();
                $t->string('CardHolder', 255)->nullable();
                $t->string('CardBrand', 20)->nullable();
                $t->string('CardNumber', 20)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // We're not going to drop the table anymore because it has lots of records
        // The records can be recreated but it takes a long time in production.
        // Schema::drop('PaymentGatewayTransaction');
    }
}
