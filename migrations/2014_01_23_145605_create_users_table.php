<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('User', function ($table) {
            $table->increments('UserId');
            $table->string('Password');
            $table->string('Email');
            $table->string('Name');
            $table->boolean('LocalAuth')->nullable();
            $table->string('Locale');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('User');
    }
}
