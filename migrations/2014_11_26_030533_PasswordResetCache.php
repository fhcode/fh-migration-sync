<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PasswordResetCache extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('PasswordResetCache', function (Blueprint $table) {
            $table->string('CacheKey', 36);
            $table->timestamp('ExpirationTs');
            $table->bigInteger('ResponderId')->default(0);
            $table->string('Email', 255);
            $table->timestamps();
            $table->primary('CacheKey');
        });
        Schema::dropIfExists('TempKey');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('PasswordResetCache');
        Schema::create('TempKey', function (Blueprint $table) {
            $table->increments('TempKeyId');
            $table->string('Email', 255);
            $table->string('Hash',36);
            $table->timestamps();
            $table->softDeletes();
        });
	}

}
