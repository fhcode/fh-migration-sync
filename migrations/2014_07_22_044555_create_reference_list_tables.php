<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferenceListTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ReferenceList', function (Blueprint $table) {
            $table->increments('ReferenceListId');
            $table->integer('ParentId')->nullable();
            $table->integer('LeftIndex')->nullable();
            $table->integer('RightIndex')->nullable();
            $table->integer('Depth')->nullable();

            $table->string('Description', 250);
            $table->string('Code', 20);

            $table->boolean('Active')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });



        Schema::create('ReferenceListTranslation', function (Blueprint $table) {
            $table->increments('ReferenceListTranslationId');
            $table->integer('ReferenceListId')->unsigned();
            $table->string('Description');
            $table->string('Locale')->index();
            $table->unique(['ReferenceListId', 'Locale']);
            $table->foreign('ReferenceListId')->references('ReferenceListId')->on('ReferenceList')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ReferenceListTranslation', function (Blueprint $table) {
            $table->dropForeign('referencelisttranslation_referencelistid_foreign');
        });


        Schema::drop('ReferenceList');
        Schema::drop('ReferenceListTranslation');
    }

}
