<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Fh\Data\Dao\US\ReferenceList;

class InsertCostShareTypeReferenceListData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
         * translations are not provided
         */
        $list = [
            ["Cash", "Cash"],
            ["GIK","GIK"]
        ];
        $root = ReferenceList::where('Code', '=', 'CostShareType')->first();
        if(!is_object($root))
        {
            $root = new ReferenceList();
            $root->Description = 'Cost Share Type';
            $root->Code = 'CostShareType';
            $root->Active = true;

            $root->save();
        }



        foreach ($list as $type) {

            $child = ReferenceList::where('Description', '=', $type[0])->where('ParentId', '=', $root->ReferenceListId)->first();

            if(!is_object($child))
            {

                $child = new ReferenceList();

                $child->Description = $type[0];
                $child->Depth = 1;
                $child->Active = true;

                $child->save();

                $child->makeChildOf($root);

                $child->translateOrNew('en')->Description = $type[0];
                //$child->translateOrNew('es')->Description = $type[1];

                $child->save();
            }
        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        $list = [
            "Cash",
            "GIK"
        ];

        $root = ReferenceList::where('Code', '=', 'CostShareType')->first();

        if(is_object($root))
        {
            foreach ($list as $type) {
                $child = ReferenceList::where('Description', '=', '')->where('ParentId', '=', $root->ReferenceListId)->first();

                if(is_object($child))
                {
                    ReferenceListTranslation::where('ReferenceListId','=', $child->ReferenceListId)->where('Description', '=', $type)->delete();
                    $child->delete();
                }

            }
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
