<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrantAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('GrantArea')){
            Schema::create('GrantArea', function (Blueprint $table) {

                $table->increments('id');
                $table->integer('GrantId');
                $table->integer('AreaId');

                $table->unique(array('GrantId', 'AreaId'));
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('GrantArea');
    }
}
