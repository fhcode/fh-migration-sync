<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGrantProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('GrantProject', function (Blueprint $table) {

            if(Schema::hasColumn('GrantProject', 'MaleBeneficiaries'))
                $table->dropColumn('MaleBeneficiaries');

            if(Schema::hasColumn('GrantProject', 'FemaleBeneficiaries'))
                $table->dropColumn('FemaleBeneficiaries');

            if(Schema::hasColumn('GrantProject', 'ChildBeneficiaries'))
                $table->dropColumn('ChildBeneficiaries');

            if(Schema::hasColumn('GrantProject', 'DataCollectionAreaType'))
                $table->dropColumn('DataCollectionAreaType');

            if(!Schema::hasColumn('GrantProject', 'MaleChildBeneficiaries'))
                $table->integer('MaleChildBeneficiaries')->unsigned()->nullable();

            if(!Schema::hasColumn('GrantProject', 'MaleAdultBeneficiaries'))
                $table->integer('MaleAdultBeneficiaries')->unsigned()->nullable();

            if(!Schema::hasColumn('GrantProject', 'FemaleChildBeneficiaries'))
                $table->integer('FemaleChildBeneficiaries')->unsigned()->nullable();

            if(!Schema::hasColumn('GrantProject', 'FemaleAdultBeneficiaries'))
                $table->integer('FemaleAdultBeneficiaries')->unsigned()->nullable();

            if(!Schema::hasColumn('GrantProject', 'IndicatorConsolidationTypes'))
                $table->string('IndicatorConsolidationTypes', 100);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('GrantProject', function (Blueprint $table) {

            if(!Schema::hasColumn('GrantProject', 'MaleBeneficiaries'))
                $table->integer('MaleBeneficiaries');

            if(!Schema::hasColumn('GrantProject', 'FemaleBeneficiaries'))
                $table->integer('FemaleBeneficiaries');

            if(!Schema::hasColumn('GrantProject', 'ChildBeneficiaries'))
                $table->integer('ChildBeneficiaries');

            if(!Schema::hasColumn('GrantProject', 'DataCollectionAreaType'))
                $table->string('DataCollectionAreaType');

            if(Schema::hasColumn('GrantProject', 'MaleChildBeneficiaries'))
                $table->dropColumn('MaleChildBeneficiaries');

            if(Schema::hasColumn('GrantProject', 'MaleAdultBeneficiaries'))
                $table->dropColumn('MaleAdultBeneficiaries');

            if(Schema::hasColumn('GrantProject', 'FemaleChildBeneficiaries'))
                $table->dropColumn('FemaleChildBeneficiaries');

            if(Schema::hasColumn('GrantProject', 'FemaleAdultBeneficiaries'))
                $table->dropColumn('FemaleAdultBeneficiaries');

            if(Schema::hasColumn('GrantProject', 'IndicatorConsolidationTypes'))
                $table->dropColumn('IndicatorConsolidationTypes');

        });
    }
}
