<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormTables extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Form', function (Blueprint $table) {
            $table->increments('FormId');
            $table->string('Name', 255);
            $table->text('Description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('FormField', function (Blueprint $table) {
            $table->increments('FormFieldId');
            $table->integer('FormId')->unsigned();
            $table->text('Label')->nullable();
            $table->integer('SortOrder')->unsigned();
            $table->string('FieldTypeName', 25);
            $table->string('DataType', 25);
            $table->boolean('IsActive');
            $table->string('RequiredMessage', 1024);
            $table->string('DefaultValue', 255);
            $table->string('Description', 255);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('FormId')->references('FormId')->on('Form');
        });

        Schema::create('FormSubmission', function (Blueprint $table) {
            $table->increments('FormSubmissionid');
            $table->integer('FormId')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('FormId')->references('FormId')->on('Form');
        });

        Schema::create('FormSubmissionData', function (Blueprint $table) {
            $table->increments('FormSubmissionDataId');
            $table->integer('FormSubmissionId')->unsigned();
            $table->integer('FormFieldId')->unsigned();
            $table->bigInteger('NumericValue')->nullable();
            $table->boolean('BooleanValue')->nullable();
            $table->string('StringValue', 36864)->nullable();
            $table->text('TextValue')->nullable();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('FormSubmissionId')->references('FormSubmissionId')->on('FormSubmission');
            $table->foreign('FormFieldId')->references('FormFieldId')->on('FormField');
        });

        Schema::create('FormFieldOption', function (Blueprint $table) {
            $table->increments('FormFieldOptionId');
            $table->integer('FormFieldId')->unsigned();
            $table->integer('OptionOrder')->unsigned();
            $table->boolean('IsActive');
            $table->string('OptionText', 1024);
            $table->string('OptionValue', 1024);

            $table->foreign('FormFieldId')->references('FormFieldId')->on('FormField');
        });
        
        Schema::create('GrantForm', function (Blueprint $table) {
            $table->increments('GrantFormId');
            $table->integer('GrantId')->unsigned();
            $table->integer('FormId')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('GrantId')->references('GrantId')->on('Grant');
            $table->foreign('FormId')->references('FormId')->on('Form');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('Form');
        Schema::drop('FormField');
        Schema::drop('FormSubmission');
        Schema::drop('FormSubmissionData');
        Schema::drop('FormFieldOption');
        Schema::drop('GrantForm');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
