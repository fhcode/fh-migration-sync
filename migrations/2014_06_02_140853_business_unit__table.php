<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BusinessUnitTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('BusinessUnit', function(Blueprint $table)
		{
			$table->increments('BusinessUnitId');
            $table->string('BusinessUnitName');
			$table->timestamps();
		});

        Schema::table('Office', function($table)
            {
                $table->foreign('BusinessUnitId')->references('BusinessUnitId')->on('BusinessUnit');
            });

        Schema::table('Worker', function($table)
            {
                $table->foreign('HostBusinessUnitId')->references('BusinessUnitId')->on('BusinessUnit');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('Office', function (Blueprint $table) {
                $table->dropForeign('office_businessunitid_foreign');
            });

        Schema::table('Worker', function (Blueprint $table) {
                $table->dropForeign('worker_hostbusinessunitid_foreign');
            });

		Schema::drop('BusinessUnit');
	}

}
