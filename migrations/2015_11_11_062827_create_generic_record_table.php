<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenericRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('GenericRecord')){
            Schema::create('GenericRecord', function (Blueprint $table) {
                $table->increments('GenericRecordId');
                $table->integer('ParentRecordId');
                $table->string("ParentTableName",60);
                $table->string("Type",60);
                $table->dateTime("ValueDate1")->nullable();
                $table->string("ValueText1",300)->nullable();
                $table->decimal("ValueDecimal1",19,2)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        if(Schema::hasTable('GenericRecord'))
        {
            Schema::dropIfExists('GenericRecord');
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
