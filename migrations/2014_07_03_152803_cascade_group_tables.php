<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CascadeGroupTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CascadeGroup', function (Blueprint $table) {
            $table->integer('CascadeGroupId')->unsigned();
            $table->integer('PromoterId')->unsigned();
            $table->integer('VolunteerId')->unsigned();
            $table->integer('AreaId')->unsigned();
            $table->decimal('Latitude', 18, 15)->nullable();
            $table->decimal('Longitude', 18, 15)->nullable();
            $table->timestamps();

            $table->primary('CascadeGroupId');
            $table->foreign('AreaId')->references('AreaId')->on('Area');
            $table->foreign('PromoterId')->references('WorkerId')->on('Worker');
            $table->foreign('VolunteerId')->references('WorkerId')->on('Worker');

        });

        Schema::create('NeighborGroup', function (Blueprint $table) {
            $table->integer('NeighborGroupId')->unsigned();
            $table->integer('CascadeGroupId')->unsigned();
            $table->integer('LeaderId')->unsigned();
            $table->timestamps();

            $table->primary('NeighborGroupId');
            $table->foreign('CascadeGroupId')->references('CascadeGroupId')->on('CascadeGroup');
            $table->foreign('LeaderId')->references('WorkerId')->on('Worker');
        });

        Schema::create('Household', function (Blueprint $table) {
            $table->integer('HouseholdId')->unsigned();
            $table->integer('NeighborGroupId')->unsigned()->nullable();
            $table->integer('AreaId')->unsigned()->nullable();
            $table->decimal('Latitude', 18, 15)->nullable();
            $table->decimal('Longitude', 18, 15)->nullable();
            $table->timestamps();

            $table->primary('HouseholdId');
            $table->foreign('NeighborGroupId')->references('NeighborGroupId')->on('NeighborGroup');
            $table->foreign('AreaId')->references('AreaId')->on('Area');
        });

        Schema::create('HHMember', function (Blueprint $table) {
            $table->integer('HHMemberId')->unsigned();
            $table->integer('HouseholdId')->unsigned();
            $table->string('MemberName', 100);
            $table->string('Gender', 1);
            $table->boolean('IsHeadOfHousehold')->nullable();
            $table->boolean('IsSponsored')->nullable();
            $table->boolean('CanSponsor')->nullable();
            $table->boolean('IsGuardian')->nullable();
            $table->boolean('IsParent')->nullable();
            $table->dateTime('DateRegistered');
            $table->timestamps();

            $table->primary('HHMemberId');
            $table->foreign('HouseholdId')->references('HouseholdId')->on('Household');
        });

        Schema::create('TrainingRecord', function (Blueprint $table) {
            $table->integer('TrainingRecordId')->unsigned();
            $table->integer('HHMemberId')->nullable()->unsigned();
            $table->integer('ParticipantId')->nullable()->unsigned();
            $table->integer('LessonListId')->unsigned();
            $table->dateTime('TrainingDate');
            $table->timestamps();

            $table->primary('TrainingRecordId');
            $table->foreign('HHMemberId')->references('HHMemberId')->on('HHMember');
        });


        Schema::create('LessonList', function (Blueprint $table) {
            $table->integer('LessonListId')->unsigned();
            $table->string('LessonType', 20);
            $table->string('LessonName', 45);
            $table->string('Version', 45)->nullable();
            $table->string('Description', 250)->nullable();
            $table->string('Sector', 45)->nullable();
            $table->timestamps();

            $table->primary('LessonListId');
        });

        Schema::table('TrainingRecord', function (Blueprint $table) {
                $table->foreign('LessonListId')->references('LessonListId')->on('LessonList');
        });

        Schema::create('ElementList', function (Blueprint $table) {
            $table->integer('ElementListId')->unsigned();
            $table->string('Name', 45);
            $table->string('Description', 250)->nullable();
            $table->timestamps();

            $table->primary('ElementListId');
        });

        Schema::create('ElementInstall', function (Blueprint $table) {
            $table->integer('ElementInstallId')->unsigned();
            $table->integer('HouseholdId')->unsigned();
            $table->integer('ElementListId')->unsigned();
            $table->dateTime('InstallDate');
            $table->timestamps();

            $table->primary('ElementInstallId');
            $table->foreign('HouseholdId')->references('HouseHoldId')->on('Household');
            $table->foreign('ElementListId')->references('ElementListId')->on('ElementList');

        });

        Schema::create('Participant', function (Blueprint $table) {
            $table->integer('ParticipantId')->unsigned();
            $table->integer('NeighborGroupId')->unsigned();
            $table->string('ParticipantName', 100);
            $table->string('Gender', 10);
            $table->dateTime('StartDate')->nullable();
            $table->timestamps();

            $table->primary('ParticipantId');
            $table->foreign('NeighborGroupId')->references('NeighborGroupId')->on('NeighborGroup');
        });

        Schema::table('TrainingRecord', function (Blueprint $table) {
            $table->foreign('ParticipantId')->references('ParticipantId')->on('Participant');
        });

        Schema::create('TrainingGroup', function (Blueprint $table) {
            $table->integer('TrainingGroupId')->unsigned();
            $table->integer('AreaId')->unsigned()->nullable();
            $table->string('GroupType');
            $table->string('GroupDescription', 250)->nullable();
            $table->dateTime('StartDate');
            $table->integer('LeaderId')->unsigned()->nullable();
            $table->string('LeaderType', 20)->nullable();
            $table->timestamps();

            $table->primary('TrainingGroupId');
            $table->foreign('AreaId')->references('AreaId')->on('Area');
            $table->foreign('LeaderId')->references('WorkerId')->on('Worker');
        });

        Schema::create('TrainingGroupMember', function (Blueprint $table) {
            $table->integer('TrainingGroupMemberId')->unsigned();
            $table->integer('TrainingGroupId')->unsigned();
            $table->integer('MemberId')->unsigned();
            $table->string('MemberType', 20);

            $table->primary('TrainingGroupMemberId');
            $table->foreign('TrainingGroupId')->references('TrainingGroupId')->on('TrainingGroup');
        });


        Schema::create('TrainingMemo', function (Blueprint $table) {
            $table->integer('TrainingMemoId')->unsigned();
            $table->integer('TrainingRecordId')->unsigned();
            $table->string('MemoType', 20);
            $table->string('MemoData', 250)->nullable();
            $table->timestamps();

            $table->primary('TrainingMemoId');
            $table->foreign('TrainingRecordId')->references('TrainingRecordId')->on('TrainingRecord');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CascadeGroup', function (Blueprint $table) {
            $table->dropForeign('cascadegroup_promoterid_foreign');
            $table->dropForeign('cascadegroup_areaid_foreign');
        });

        Schema::table('Household', function (Blueprint $table) {
            $table->dropForeign('household_areaid_foreign');
            $table->dropForeign('household_neighborgroupid_foreign');
        });

        Schema::table('NeighborGroup', function (Blueprint $table) {
            $table->dropForeign('neighborgroup_cascadegroupid_foreign');
        });

        Schema::table('ElementInstall', function (Blueprint $table) {
            $table->dropForeign('elementinstall_elementlistid_foreign');
            $table->dropForeign('elementinstall_householdid_foreign');
        });


        Schema::table('TrainingGroup', function (Blueprint $table) {
            $table->dropForeign('traininggroup_areaid_foreign');
        });

        Schema::table('TrainingGroupMember', function (Blueprint $table) {
                $table->dropForeign('traininggroupmember_traininggroupid_foreign');
        });

        Schema::table('HHMember', function (Blueprint $table) {
            $table->dropForeign('hhmember_householdid_foreign');
        });

        Schema::table('TrainingRecord', function (Blueprint $table) {
            $table->dropForeign('trainingrecord_participantid_foreign');
            $table->dropForeign('trainingrecord_hhmemberid_foreign');
            $table->dropForeign('trainingrecord_lessonlistid_foreign');
        });

        Schema::table('TrainingMemo', function (Blueprint $table) {
                $table->dropForeign('trainingmemo_trainingrecordid_foreign');
        });

        Schema::table('Participant', function (Blueprint $table) {
                $table->dropForeign('participant_neighborgroupid_foreign');
        });

        Schema::drop('CascadeGroup');
        Schema::drop('NeighborGroup');
        Schema::drop('Household');
        Schema::drop('HHMember');
        Schema::drop('TrainingRecord');
        Schema::drop('LessonList');
        Schema::drop('ElementList');
        Schema::drop('ElementInstall');
        Schema::drop('Participant');
        Schema::drop('TrainingGroup');
        Schema::drop('TrainingGroupMember');
        Schema::drop('TrainingMemo');
    }

}
