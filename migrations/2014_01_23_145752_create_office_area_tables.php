<?php

use Illuminate\Database\Migrations\Migration;
use Fh\ApiConfig\ApacheConfigHelper;
use Illuminate\Database\Schema\Blueprint;

class CreateOfficeAreaTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('Area', function (Blueprint $table) {
                $table->increments('AreaId')->unsigned();
                $table->integer('ParentId')->nullable();
                $table->integer('LeftIndex')->nullable();
                $table->integer('RightIndex')->nullable();
                $table->integer('Depth')->nullable();
                $table->string('AreaName', 255);
                $table->string('TypeName', 20);
                $table->timestamps();
                $table->softDeletes();

                $table->index('ParentId');
                $table->index('LeftIndex');
                $table->index('RightIndex');
            });


        // Creates the Office table
        Schema::create('Office', function (Blueprint $table) {
            $table->increments('OfficeId')->unsigned();
            $table->string('OfficeName');
            $table->integer('AreaId')->unsigned();
            $table->foreign('AreaId')->references('AreaId')->on('Area');
            $table->integer('BusinessUnitId')->unsigned()->nullable();
            $table->string('City');
            $table->string('Address');
            $table->string('Phone');
            $table->decimal('Latitude', 18, 15)->nullable();
            $table->decimal('Longitude', 18, 15)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Office');
        Schema::drop('Area');

    }
}
