<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Fh\Data\Mapper\US\AllowedParentAreaMapper;
use Fh\Data\Mapper\US\AreaMapper;
class InsertAllowedParentArea extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
         * this data is for testing purpose only --- specific list must be entered using the system form.
         */
        $ethiopia = AreaMapper::where('AreaName', '=', "Ethiopia")->first();
        if(is_object($ethiopia))
        {

            $parentArea = new AllowedParentAreaMapper();
            $parentArea->AreaTypeCode = "Woreda";
            $parentArea->ParentAreaId = $ethiopia->AreaId;

            $result = $parentArea->save();
        }
        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        $ethiopia = AreaMapper::where('AreaName', '=', "Ethiopia")->first();

        if(is_object($ethiopia))
        {

            AllowedParentAreaMapper::where('AreaTypeCode','=', "Woreda")->where("ParentAreaId",'=',$ethiopia->AreaId)->delete();
        }


        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
