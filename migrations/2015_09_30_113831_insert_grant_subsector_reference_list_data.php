<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Fh\Data\Dao\US\ReferenceList;

class InsertGrantSubsectorReferenceListData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
         * translations are not provided
         */
        $list = [
            ["DRR","DRR"],
            ["Gender","Gender"],
            ["Agriculture","Agriculture"],
            ["Eco Develop","Eco Develop"],
            ["Education","Education"],
            ["Livestock","Livestock"],
            ["NRM","NRM"],
            ["Nutrition","Nutrition"],
            ["Savings Groups","Savings Groups"],
            ["Value Chains","Value Chains"],
            ["WASH","WASH"],
            ["Child Survival","Child Survival"],
            ["HIV/AIDS","HIV/AIDS"],
            ["Hygiene","Hygiene"],
            ["MCHN","MCHN"],
            ["SBC","SBC"],
            ["Resilience","Resilience"]
        ];
        $root = ReferenceList::where('Code', '=', 'GrantSubSector')->first();
        if(!is_object($root))
        {
            $root = new ReferenceList();
            $root->Description = 'Grant SubSector';
            $root->Code = 'GrantSubSector';
            $root->Active = true;

            $root->save();
        }

        //$root = ReferenceList::where('Description', '=', 'Cost Payment Type')->first();

        foreach ($list as $type) {

            $child = ReferenceList::where('Description', '=', $type[0])->where('ParentId', '=', $root->ReferenceListId)->first();

            if(!is_object($child))
            {

                $child = new ReferenceList();

                $child->Description = $type[0];
                $child->Depth = 1;
                $child->Active = true;

                $child->save();

                $child->makeChildOf($root);

                $child->translateOrNew('en')->Description = $type[0];
                //$child->translateOrNew('es')->Description = $type[0];

                $child->save();
            }
        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        $list = [
            "DRR",
            "Gender",
            "Agriculture",
            "Eco Develop",
            "Education",
            "Livestock",
            "NRM",
            "Nutrition",
            "Savings Groups",
            "Value Chains",
            "WASH",
            "Child Survival",
            "HIV/AIDS",
            "Hygiene",
            "MCHN",
            "SBC",
            "Resilience"
        ];

        $root = ReferenceList::where('Code', '=', 'GrantSubSector')->first();

        if(is_object($root))
        {
            foreach ($list as $type) {
                $child = ReferenceList::where('Description', '=', '')->where('ParentId', '=', $root->ReferenceListId)->first();

                if(is_object($child))
                {
                    ReferenceListTranslation::where('ReferenceListId','=', $child->ReferenceListId)->where('Description', '=', $type)->delete();
                    $child->delete();
                }

            }
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
