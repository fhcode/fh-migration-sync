<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('AreaType')){
            Schema::create('AreaType', function (Blueprint $table) {
                $table->string('AreaTypeCode', 30)->primary();
                $table->string('TypeName', 50)->unique();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        if(!Schema::hasTable('AllowedParentAreaType'))
        {
            Schema::dropIfExists('AreaType');
        }
        else
        {
            Schema::dropIfExists('AllowedParentAreaType');
            Schema::dropIfExists('AreaType');
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
