<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrantOrganizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('GrantOrganization'))
        {
            Schema::create('GrantOrganization', function (Blueprint $table) {

                $table->increments('id');
                $table->integer('OrganizationId');
                $table->integer('GrantId');
                $table->string('RelationshipType', 20);

                $table->unique(array('OrganizationId', 'GrantId', 'RelationshipType'));
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('GrantOrganization');
    }
}
