<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserWorkerMods extends Migration {

    /**
     * Run the migrations.
     *
     * We decided on Jan 26, 2015 that All users must have workers, but not
     * all workers must have users. - Brenda, Kevin, and also that guy Jon
     *
     * @return void
     */
    public function up()
    {
        Schema::table('User', function (Blueprint $table) {
            $table->dropColumn('LocalAuth');
            $table->dropColumn('Name');
            $table->integer('WorkerId')->unsigned()->nullable();
            $table->foreign('WorkerId')->references('WorkerId')->on('Worker');
        });
        DB::statement('UPDATE User, Worker SET User.WorkerId = Worker.WorkerId WHERE Worker.UserId = User.UserId');
        Schema::table('Worker', function (Blueprint $table) {
            $table->dropForeign('worker_userid_foreign');
            $table->dropColumn('UserId');
            $table->dropColumn('WorkerType');
            $table->enum('AccountType',[
                'systems'
                ,'contractor/volunteer'
                ,'staff'
            ]);
        });
        DB::statement('UPDATE Worker SET Worker.AccountType = ?',['staff']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Worker', function (Blueprint $table) {
            $table->integer('UserId')->unsigned()->nullable();
            $table->string('WorkerType',15);
            $table->foreign('UserId')->references('UserId')->on('User');
            $table->dropColumn('AccountType');
        });
        DB::statement('UPDATE User, Worker SET Worker.UserId = User.UserId WHERE User.WorkerId = Worker.WorkerId');
        Schema::table('User', function (Blueprint $table) {
            $table->tinyInteger('LocalAuth')->nullable();
            $table->string('Name',255)->nullable();
            $table->dropForeign('user_workerid_foreign');
            $table->dropColumn('WorkerId');
        });
    }

}
