<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUnusedTables extends Migration
{

    public $tables =
        [
            'CascadeGroup'
           ,'ElementInstall'
           ,'ElementList'
           ,'FormFieldOption'
           ,'FormField'
           ,'Form'
           ,'FormSubmissionData'
           ,'FormSubmission'
           ,'GrantForm'
           ,'HHMember'
           ,'Household'
           ,'LessonList'
           ,'NeighborGroup'
           ,'Participant'
           ,'TrainingGroupMember'
           ,'TrainingGroup'
           ,'TrainingMemo'
           ,'TrainingRecord'
        ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $table) {
            $this->removeTable($table);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    public function removeTable($table)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists($table);
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
