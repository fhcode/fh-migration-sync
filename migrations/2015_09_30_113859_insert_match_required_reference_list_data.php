<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Fh\Data\Dao\US\ReferenceList;

class InsertMatchRequiredReferenceListData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $list = [
            ["Yes", 'Yes'],
            ["No", 'No'],
            ["Recommended",'Recommended']
        ];
        $root = ReferenceList::where('Code', '=', 'MatchRequired')->first();
        if(!is_object($root))
        {
            $root = new ReferenceList();
            $root->Description = 'MatchRequired';
            $root->Code = 'MatchRequired';
            $root->Active = true;

            $root->save();
        }

        //$root = ReferenceList::where('Description', '=', 'Cost Payment Type')->first();

        foreach ($list as $type) {

            $child = ReferenceList::where('Description', '=', $type[0])->where('ParentId', '=', $root->ReferenceListId)->first();

            if(!is_object($child))
            {

                $child = new ReferenceList();

                $child->Description = $type[0];
                $child->Depth = 1;
                $child->Active = true;

                $child->save();

                $child->makeChildOf($root);

                $child->translateOrNew('en')->Description = $type[0];
                //$child->translateOrNew('es')->Description = $type[1];

                $child->save();
            }
        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        $list = [
            ["Yes", 'Yes'],
            ["No", 'No'],
            ["Recommended",'Recommended']
        ];

        $root = ReferenceList::where('Code', '=', 'LeadUnit')->first();

        if(is_object($root))
        {
            foreach ($list as $type) {
                $child = ReferenceList::where('Description', '=', '')->where('ParentId', '=', $root->ReferenceListId)->first();

                if(is_object($child))
                {
                    ReferenceListTranslation::where('ReferenceListId','=', $child->ReferenceListId)->where('Description', '=', $type[0])->delete();
                    $child->delete();
                }

            }
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
