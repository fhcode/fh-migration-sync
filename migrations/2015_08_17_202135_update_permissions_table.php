<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Fh\Data\Gbo\BusinessObjects\App;
use Fh\Data\Gbo\BusinessObjects\Permission;

class UpdatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //add API app
        if (!App::where('Title', 'API')->first()) {
            $apiApp = new App();
            $apiApp->Title = 'API';
            $apiApp->Code = 'API';
            $apiApp->save();
        }

        //add WL3 app
        if (!App::where('Title', 'WL3')->first()) {
            $apiApp = new App();
            $apiApp->Title = 'WL3';
            $apiApp->Code = 'WL';
            $apiApp->save();
        }


        Schema::table('permissions', function (Blueprint $table) {
                $table->integer('AppId')->unsigned();
            });

        $permissions = Permission::all();

        if (count($permissions) > 0) {
            foreach ($permissions as $permission) {
                $app = substr($permission->name,0,2);
                switch ($app) {
                    case 'gr':
                        $app= App::where('Title', 'Grants')->first();
                        break;
                    case 'ad':
                        $app= App::where('Title', 'Admin')->first();
                        break;
                    case 'ma':
                        $app= App::where('Title', 'WL3')->first();
                        break;
                    default:
                        $app= App::where('Title', 'Grants')->first();
                }
                $permission->AppId = $app->AppId;
                $permission->save();
            }
        }



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('permissions', function (Blueprint $table) {
                $table->dropColumn('AppId');
            });

        App::where('Title', 'API')->first()->delete();

    }
}
