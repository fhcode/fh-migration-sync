<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleCanGrantTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('RoleCanGrant', function (Blueprint $table) {
            $table->increments('RoleCanGrantId');
            $table->integer('RoleId')->unsigned();
            $table->integer('CanGrantRoleId')->unsigned();
            $table->timestamps();
            
            $table->foreign('RoleId')->references('id')->on('roles');
            $table->foreign('CanGrantRoleId')->references('id')->on('roles');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('RoleCanGrant');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}
}
