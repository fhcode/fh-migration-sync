<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Fh\Data\Mapper\US\AllowedParentAreaTypeMapper;
class InsertAllowedParentAreaTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $areaTypes = [
            ["Globe", 'Globe'],
            ["GlobalRegion", 'Globe'],
            ["Country",'GlobalRegion'],
            ["Cluster", "Country"],
            ["Community", "Cluster"],
            ["Woreda", "Country"]
        ];

        foreach ($areaTypes as $type) {
            $exist = AllowedParentAreaTypeMapper::where('AreaTypeCode', '=', $type[0])->exists();

            if(!$exist)
            {
                $areaType = new AllowedParentAreaTypeMapper();
                $areaType->AreaTypeCode = $type[0];
                $areaType->ParentAreaTypeCode = $type[1];

                $areaType->save();
            }
        }

        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        $areaTypes = [
            ["Globe", 'Globe'],
            ["GlobalRegion", 'Globe'],
            ["Country",'GlobalRegion'],
            ["Cluster", "Country"],
            ["Community", "Cluster"],
            ["Woreda", "Country"]
        ];

        foreach ($areaTypes as $type) {
            $exist = AllowedParentAreaTypeMapper::where('AreaTypeCode', '=', $type[0])->exists();

            if($exist)
            {
                AllowedParentAreaTypeMapper::where('AreaTypeCode','=', $type[0])->delete();
            }
        }


        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
