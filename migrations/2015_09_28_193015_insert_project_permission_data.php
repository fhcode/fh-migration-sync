<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Fh\Data\Gbo\BusinessObjects\Role;
use Fh\Data\Gbo\BusinessObjects\Permission;
use Fh\Data\Gbo\BusinessObjects\PermissionRole;

class InsertProjectPermissionData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $states = [
            ['main.projects', 'Projects'],
            ['main.projectsCreate', 'Project Create'],
            ['main.projectsDetail', 'Project Detail']
        ];

        $roles = [
            ['Super Admin', 'Super Admin'],
            ['gr.Administrator', 'Grant Administrator'],
            ['gr.Manager', 'Grant Manager'],
            ['gr.Contributor', 'Grant Contributor'],
            ['Grant Administrator', 'Grant Administrator'],
            ['Grant Contributor', 'Grant Contributor']
        ];

        foreach ($states as $type)
        {

            $p = Permission::where('name','=',$type)->first();
            if(!is_object($p))
            {
                $p = new Permission();

                $p->name = $type[0];
                $p->display_name = $type[1];
                $p->created_at = '2015-05-05 00:00:00';
                $p->updated_at = '2015-05-05 00:00:00';

                $p->save();
            }

            foreach($roles as $rolename)
            {
                $role = Role::where('name', '=', $rolename[0])->first();
                if(!is_object($role))
                {
                    $role = new Role();
                    $role->name = $rolename[0];
                    $role->display_name = $rolename[1];
                    $role->created_at = '2015-05-05 00:00:00';
                    $role->updated_at = '2015-05-05 00:00:00';
                    $role->save();
                }

                $rp = PermissionRole::where('permission_id','=',$p->id)->where('role_id','=',$role->id)->first();
                if(!is_object($rp)){
                    $rp = new PermissionRole();

                    $rp->permission_id = $p->id;
                    $rp->role_id = $role->id;
                    $rp->created_at = '2017-05-05 00:00:00';
                    $rp->updated_at = '2017-05-05 00:00:00';

                    $rp->save();
                }

            }

        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $states = [
            ['main.projects', 'Projects'],
            ['main.projectsCreate', 'Project Create'],
            ['main.projectsDetail', 'Project Detail']
        ];

        $roles = [
            ['Super Admin', 'Super Admin'],
            ['gr.Administrator', 'Grant Administrator'],
            ['gr.Manager', 'Grant Manager'],
            ['gr.Contributor', 'Grant Contributor'],
            ['Grant Administrator', 'Grant Administrator'],
            ['Grant Contributor', 'Grant Contributor']
        ];

        foreach ($states as $type)
        {

            $p = Permission::where('name', '=', $type[0])->first();
            foreach($roles as $rolename)
            {

                $role = Role::where('name', '=', $rolename[0])->first();
                PermissionRole::where('permission_id','=',$p->id)->where('role_id','=',$role->id)->delete();

            }
            //Permission::where('id','=',$p->id)->delete();
        }

        DB::commit();
    }
}
