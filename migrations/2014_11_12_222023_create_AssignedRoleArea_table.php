<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignedRoleAreaTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('AssignedRoleArea', function (Blueprint $table) {
            $table->increments('AssignedRoleAreaId');
            $table->integer('AssignedRoleId')->unsigned();
            $table->integer('AreaId')->unsigned();
            $table->timestamps();
            
            $table->foreign('AssignedRoleId')->references('id')->on('assigned_roles');
            $table->foreign('AreaId')->references('AreaId')->on('Area');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('AssignedRoleArea');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}
}
