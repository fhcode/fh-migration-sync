<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveAwardAmountFromGrantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Grant', function (Blueprint $table) {

            if(Schema::hasColumn('Grant', 'Amount'))
                $table->dropColumn('Amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Grant', function (Blueprint $table) {

            if(!Schema::hasColumn('Grant', 'Amount'))
                $table->decimal('Amount',19,2)->nullable();
        });
    }
}
