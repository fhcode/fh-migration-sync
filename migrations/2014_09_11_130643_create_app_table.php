<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App', function(Blueprint $table)
        {
            $table->increments('AppId');
            $table->string('Title', 25);
            $table->string('Code', 3);
            $table->timestamps();
        });

        Schema::table('ManagedRoute', function (Blueprint $table) {
                $table->integer('AppId')->unsigned();
                $table->foreign('AppId')->references('AppId')->on('App');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ManagedRoute', function (Blueprint $table) {
            $table->dropForeign('managedroute_appid_foreign');
        });

        Schema::dropIfExists('App');
    }

}
