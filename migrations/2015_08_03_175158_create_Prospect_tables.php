<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
//use Fh\Data\Console\Seed\DataSeeder;
use Fh\Data\Gbo\BusinessObjects\Role;
use Fh\Data\Gbo\BusinessObjects\Permission;
use Fh\Data\Gbo\BusinessObjects\ReferenceList;
//use Fh\Data\Mapper\US\ReferenceListMapper;
//use Fh\Data\Dao\US\ReferenceList;

class CreateProspectTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('NdaProspect')) {
            Schema::create('NdaProspect', function (Blueprint $table) {
                    $table->increments('ProspectId');
                    $table->string('Title', 20)->nullable();
                    $table->string('FirstName', 20);
                    $table->string('LastName', 20);
                    $table->string('Suffix', 20)->nullable();
                    $table->string('Organization', 70)->nullable();
                    $table->string('Address1', 70);
                    $table->string('Address2', 70)->nullable();
                    $table->string('Address3', 70)->nullable();
                    $table->string('City', 70);
                    $table->string('State', 10);
                    $table->string('Zipcode', 15)->nullable();
                    $table->string('Salutation', 70)->nullable();
                    $table->string('Phone', 45);
                    $table->string('SourceCode', 10)->nullable();
                    $table->string('SegCode', 10)->nullable();
                    $table->integer('DoNotContactReason')->length(10)->nullable();
                    $table->timestamps();
                    $table->softDeletes();

                    //$table->foreign('DoNotContactReason')->references('ReferenceListId')->on('ReferenceList');
                });
        }

        if (!Schema::hasTable('NdaProspectTemp')) {
            Schema::create('NdaProspectTemp', function (Blueprint $table) {
                    $table->increments('ProspectId');
                    $table->string('Title', 20)->nullable();
                    $table->string('FirstName', 20);
                    $table->string('LastName', 20);
                    $table->string('Suffix', 20)->nullable();
                    $table->string('Organization', 70)->nullable();
                    $table->string('Address1', 70);
                    $table->string('Address2', 70)->nullable();
                    $table->string('Address3', 70)->nullable();
                    $table->string('City', 70);
                    $table->string('State', 10);
                    $table->string('Zipcode', 15)->nullable();
                    $table->string('Salutation', 70)->nullable();
                    $table->string('Phone', 45);
                    $table->string('SourceCode', 10)->nullable();
                    $table->string('SegCode', 10)->nullable();
                    $table->integer('DoNotContactReason')->length(10)->nullable();
                    $table->timestamps();
                    $table->softDeletes();

                    //$table->foreign('DoNotContactReason')->references('ReferenceListId')->on('ReferenceList');
                });
        }


        //if (getenv('ENVIRONMENT') == 'development') {
          $this->setPermissionsToRole();
          $this->seedReferenceList();
        //}
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('NdaProspect');
        Schema::dropIfExists('NdaProspectTemp');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

    private function setPermissionsToRole()
    {
        //Define prospects permissions
        $Permissions = array(
            array('name' => 'main.prospects', 'display' => 'Access WL3 Prospects GUI' ),
            array('name' => 'prospects.get', 'display' => 'View Prospects'),
            array('name' => 'prospects.put', 'display' => 'Edit Prospects')
        );

        //only create the missing permissions
        foreach($Permissions as &$p) {
            $perm = Permission::where('name', '=', $p['name'] )->count();
            if($perm < 1) {
                $perm = new Permission();
                $perm->name = $p['name'];
                $perm->display_name = $p['display'];
                $perm->save();
            }
            $perm = Permission::where('name', '=', $p['name'] )->first();
            $p['id'] = $perm->id;
        }

        //Assign Permissions to Roles
        $Roles = array(
            'gsc.csr' => array( 
                'display' => 'GSC Customer Service Rep',
                'permissions' => $Permissions
            ),
            'gsc.rpt' => array(
                'display' => 'GSC Reporting',
                'permissions' => array($Permissions[1])
            )
        );

        //and Create the non-existant roles
        foreach($Roles as $name => $aRole){
            $r = Role::where('name', '=', $name)->count();
            if($r < 1){
                $newRole = new Role();
                $newRole->name = $name;
                $newRole->display_name = $aRole['display'];
                $newRole->save();
            }
            $r = Role::where('name', '=', $name)->first();
           
            $pIDs = array();
            foreach($aRole['permissions'] as &$p) {
                $pIDs[] = $p['id'];
            }
            $r->perms()->attach($pIDs);
        } 

        $pIDs = array();
        //Add Permissions to Super Admin also
        foreach($Permissions as $p) {
            $pIDs[] = $p['id'];
        }
        $r = Role::where('name', '=', 'Super Admin')->first();
        $r->perms()->attach($pIDs);

    }

    private function seedReferenceList(){
        //Seed "Do Not Contact Reasons" into Reference List Table
        $reasons = '[ {"description": "Deceased", "translation": "Fallecido", "code": "deceased"},
    {"description": "Do not Contact", "translation": "No Contactar", "code": "do_not_contact"}, 
    {"description": "Invalid Address", "translation": "Direccion no es valida", "code": "invalid_address"} ]';

        $reasons = json_decode($reasons);

        $DoNotContactReasonRoot = ReferenceList::create(['description' => 'Do Not Contact Reason', 'Code' => 'DoNotContactReason', 'Active' => true ]);
        $DoNotContactReasonRoot->translateOrNew('en')->Description = 'Do Not Contact Reason';
        $DoNotContactReasonRoot->translateOrNew('es')->Description = 'Razon de no Contactar';
        $DoNotContactReasonRoot->save();

        foreach ($reasons as $aReason) {
            //only create the ones missing.
            $exists = ReferenceList::where('Code', '=', $aReason->code)->count();
            if($exists < 1){
                $child = new ReferenceList();
                $child->Depth = 1;
                $child->Description = $aReason->description;
                $child->Code = $aReason->code;
                $child->save();
                $child->makeChildOf($DoNotContactReasonRoot->ReferenceListId);
                $child->translateOrNew('en')->Description = $aReason->description;
                $child->translateOrNew('es')->Description = $aReason->translation;
                $child->save();
            }
        } 

    }

}
