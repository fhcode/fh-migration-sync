<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAreaAreatypeColumnSize extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('Area'))
        {
            Schema::table('Area', function (Blueprint $table) {

                if(Schema::hasColumn('Area', 'TypeName'))
                {
                    /*
                     * Since there is no way of altering the column structure with the schema builder,
                     * we are obliged to use the internal DB functionality
                     */
                    DB::statement('ALTER TABLE Area MODIFY COLUMN TypeName VARCHAR(50)');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('Area'))
        {
            Schema::table('Area', function (Blueprint $table) {

                if(Schema::hasColumn('Area', 'TypeName'))
                {
                    /*
                     * Since there is no way of altering the column structure with the schema builder,
                     * we are obliged to use the internal DB functionality
                     */
                    DB::statement('ALTER TABLE Area MODIFY COLUMN TypeName VARCHAR(20)');
                }
            });
        }
    }
}
