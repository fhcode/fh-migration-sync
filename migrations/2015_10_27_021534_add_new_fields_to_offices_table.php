<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Office', function (Blueprint $table) {
            $table->text('DistanceInfo')->nullable()->after('OfficeName');
            $table->text('TravelInfo')->nullable()->after('DistanceInfo');

            $table->integer('ItWorkerId')->unsigned()->nullable()->after('TravelInfo');
            $table->integer('ReportingOfficeId')->unsigned()->nullable()->after('ItWorkerId');

            $table->string('Phone', 50)->nullable()->change();

            $table->string('Phone2', 50)->nullable()->after('Phone');
            $table->string('Fax', 50)->nullable()->after('Phone2');
            $table->string('MailingAddress', 350)->nullable()->after('Fax');

            $table->string('Address', 350)->change();
        });

        Schema::table('Office', function (Blueprint $table) {
            $table->renameColumn('Phone', 'Phone1');
            $table->renameColumn('Address', 'StreetAddress');
        });

        Schema::table('Office', function (Blueprint $table) {
            $table->string('StreetAddress')->nullable()->change();
            $table->string('City')->nullable()->change();
        });

        Schema::table('Office', function (Blueprint $table) {
            $table->foreign('ItWorkerId')->references('WorkerId')->on('Worker');

            $table->foreign('ReportingOfficeId')->references('OfficeId')->on('Office');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Office', function (Blueprint $table) {
            $table->dropColumn('DistanceInfo');
            $table->dropColumn('TravelInfo');

            $table->string('Phone1', 255)->change();

            $table->dropColumn('Phone2');
            $table->dropColumn('Fax');
            $table->dropColumn('MailingAddress');

            $table->string('StreetAddress', 255)->change();
        });

        Schema::table('Office', function (Blueprint $table) {
            $table->renameColumn('Phone1', 'Phone');
            $table->renameColumn('StreetAddress', 'Address');
        });

        Schema::table('Office', function (Blueprint $table) {
            $table->dropForeign('Office_ItWorkerId_foreign');
            $table->dropColumn('ItWorkerId');

            $table->dropForeign('Office_ReportingOfficeId_foreign');
            $table->dropColumn('ReportingOfficeId');
        });
    }
}
