<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkerTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Worker', function (Blueprint $table) {
            $table->increments('WorkerId');
            $table->integer('HostBusinessUnitId')->unsigned()->nullable();
            $table->integer('AssignedOfficeId')->unsigned()->nullable();
            $table->integer('ReportingOfficeId')->unsigned()->nullable();
            $table->string('WorkerType', 15);
            $table->integer('UserId')->unsigned()->nullable();
            $table->string('FirstName');
            $table->string('LastName');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('Worker', function (Blueprint $table) {
                $table->foreign('AssignedOfficeId')->references('OfficeId')->on('Office');
                $table->foreign('ReportingOfficeId')->references('OfficeId')->on('Office');
                $table->foreign('UserId')->references('UserId')->on('User');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Worker', function (Blueprint $table) {
                $table->dropForeign('worker_assignedofficeid_foreign');
                $table->dropForeign('worker_reportingofficeid_foreign');
                $table->dropForeign('worker_userid_foreign');
        });

        Schema::drop('Worker');
    }

}
