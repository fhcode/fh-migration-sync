<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagedRouteTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ManagedRoute', function (Blueprint $table) {
                // These columns are needed for Baum's Nested Set implementation to work.
                // Column names may be changed, but they *must* all exist and be modified
                // in the model.
                // Take a look at the model scaffold comments for details.
                $table->increments('ManagedRouteId');
                $table->integer('ParentId')->nullable();
                $table->integer('LeftIndex')->nullable();
                $table->integer('RightIndex')->nullable();
                $table->integer('Depth')->nullable();

                // Add needed columns here (f.ex: name, slug, path, etc.)
                $table->string('Title');
                $table->string('Uri')->nullable();
                $table->string('Descendants')->nullable();
                $table->string('Class')->nullable();
                $table->string('Attributes')->nullable();
                $table->string('RouteName')->nullable();
                $table->string('Controller')->default('');
                $table->string('RouteVerb')->nullable();
                $table->boolean('Hidden')->nullable();
                $table->boolean('Public');


                $table->timestamps();

                // Default indexes
                // Add indexes on parent_id, lft, rgt columns by default. Of course,
                // the correct ones will depend on the application and use case.
                $table->index('ParentId');
                $table->index('LeftIndex');
                $table->index('RightIndex');

        });

        Schema::create('ManagedRouteTranslation', function (Blueprint $table) {
            $table->increments('ManagedRouteTranslationId');
            $table->integer('ManagedRouteId')->unsigned();
            $table->string('Title');
            $table->string('Locale')->index();

            $table->timestamps();

            $table->unique(['ManagedRouteId', 'Locale']);
            $table->foreign('ManagedRouteId')->references('ManagedRouteId')->on('ManagedRoute')->onDelete('cascade');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ManagedRouteTranslation');
        Schema::drop('ManagedRoute');
    }
}
