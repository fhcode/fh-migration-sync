<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Fh\Data\Console\Seed\DataSeeder;

class OrganizationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Organization', function (Blueprint $table) {
            $table->increments('OrganizationId');
            $table->string('Name', 100);
            $table->string('Street1')->nullable();
            $table->string('Street2')->nullable();
            $table->string('City', 50)->nullable();
            $table->string('State', 2)->nullable();
            $table->string('Zip', 12)->nullable();
            $table->string('Country', 50)->nullable();
            $table->string('Phone', 50)->nullable();
            $table->string('Fax', 50)->nullable();
            $table->string('Website', 100)->nullable();
            $table->string('SunDonorCode', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('Contact', function (Blueprint $table) {
            $table->increments('ContactId');
            $table->integer('OrganizationId')->unsigned();
            $table->string('FirstName', 50);
            $table->string('LastName', 50);
            $table->string('Email', 255)->nullable();
            $table->string('Skype', 100)->nullable();
            $table->string('MainPhone', 50)->nullable();
            $table->string('CellPhone', 50)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('OrganizationId')->references('OrganizationId')->on('Organization');
        });

        if (getenv('ENVIRONMENT') == 'development') {
            $this->fillTestData();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('Organization');
        Schema::dropIfExists('Contact');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

    private function fillTestData()
    {
        $seeder = new DataSeeder();
        $seeder->importData('Organization', __DIR__ . '/Data/');
        $seeder->importData('Contact', __DIR__ . '/Data/');
    }
}
