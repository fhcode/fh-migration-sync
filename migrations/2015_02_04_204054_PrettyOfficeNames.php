<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PrettyOfficeNames extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $strSql = "UPDATE Office SET Office.OfficeName = Office.City WHERE Office.OfficeId > 0";
        DB::statement($strSql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $strSql = "UPDATE Office SET Office.City = Office.OfficeName WHERE Office.OfficeId > 0";
        DB::statement($strSql);
    }

}
