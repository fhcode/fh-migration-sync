<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixGrantDataSubmissionPolymorphismGbo extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        /*
         * When the fh/enterprisedb package was shut down, there there were some references to it in the database
         * in order to make the polymorphic relationship work with the GrantProjectPeriod.
         *
         * This migration changes those references to use the Mapper rather than the Enterprise DB
         */
        DB::table('GrantDataSubmission')
            ->where('PeriodicityType', '=', 'Fh\EnterpriseDb\Models\ReferenceList')
            ->update(['PeriodicityType' => 'Fh\Data\Mapper\US\ReferenceListMapper']);

        DB::table('GrantDataSubmission')
            ->where('PeriodicityType', '=', 'Fh\EnterpriseDb\Models\GrantProjectPeriod')
            ->update(['PeriodicityType' => 'Fh\Data\Mapper\US\GrantProjectPeriodMapper']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        DB::table('GrantDataSubmission')
            ->where('PeriodicityType', '=', 'Fh\Data\Mapper\US\ReferenceListMapper')
            ->update(['PeriodicityType' => 'Fh\EnterpriseDb\Models\ReferenceList']);

        DB::table('GrantDataSubmission')
            ->where('PeriodicityType', '=', 'Fh\Data\Mapper\US\GrantProjectPeriodMapper')
            ->update(['PeriodicityType' => 'Fh\EnterpriseDb\Models\GrantProjectPeriod']);
    }

}
