<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllowedParentAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('AllowedParentArea'))
        {
            Schema::create('AllowedParentArea', function (Blueprint $table) {

                $table->increments("AllowedParentAreaId");
                $table->string('AreaTypeCode', 10);
                $table->integer('ParentAreaId')->unsigned();
                $table->boolean("Required");

                //checking weather the foreign key is created or not is essential here
                if(Schema::hasTable('AreaType'))
                    $table->foreign('AreaTypeCode')->references('AreaTypeCode')->on('AreaType');

//                if(Schema::hasTable('Area'))
//                    $table->foreign('ParentAreaId')->references('AreaId')->on('Area');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AllowedParentArea');
    }
}
